<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 13/03/2018
 * Time: 0:32
 */
require APPPATH . 'libraries/REST_Controller.php';

class Events extends REST_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->bucket = REST_Controller::bucket_staging;
        if (strpos(base_url(), REST_Controller::api_prod) !== false) {
            $this->bucket = REST_Controller::bucket_prod;
        }
        $this->load->library("CloudStorage", NULL, "s3");
        $this->load->helper("baboo");
    }

    public function getEvent_get()
    {
        $url_event = array();
        $this->db->select("event_id, event_name, event_start, event_end, event_image,  event_sideimage, event_banner, event_redirect");
        $getEvent = $this->db->get_where("events_baboo", "NOW() >= event_start AND NOW() <= event_end");
        if ($getEvent->num_rows() > 0) {
            foreach ($getEvent->result() as $eval) {
                $redirect = $eval->event_redirect;
                //set for redirecting url
                $redirect_url = REST_Controller::app_dev . $redirect;
                if (strpos(base_url(), REST_Controller::api_staging) !== false) {
                    $redirect_url = REST_Controller::app_staging . $redirect;
                } elseif (strpos(base_url(), REST_Controller::api_prod) !== false) {
                    $redirect_url = REST_Controller::app_prod . $redirect;
                }
                //set to array
                $eval->event_redirect = $redirect_url;
                $url_event[] = $eval;
            }
        }
        $responses = array(
            "code" => REST_Controller::HTTP_OK,
            "message" => "get data event success",
            "data" => $url_event
        );
        $this->set_response($responses, REST_Controller::HTTP_OK);
    }

    public function insertEvent_post(){
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = $http_response_header;
        $data_insert = array();
        $resp_message = "insert event failed";
        $event_name = $this->post("event_name");
        $event_start = (!empty($this->post("event_start"))) ? $this->post("event_start") : date("Y-m-d H:i:s");
        $event_end = (!empty($this->post("event_end"))) ? $this->post("event_end") : date("Y-m-d H:i:s");
        if(!empty($event_name) &&!empty($event_start) && !empty($event_end) && !empty($this->post("event_redirect"))){
            $event_banner = "";
            $event_image = "";
            if (!empty($_FILES) && $_FILES["event_image"]["error"] != 4) {
                $files = $_FILES["event_image"]["name"];
                $ext = $ext = pathinfo($files, PATHINFO_EXTENSION);
                $tmpName = $_FILES["event_image"]["tmp_name"];
                $fileName = "baboo-cover/eventimage_" . time() . ".$ext";
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $event_image = $this->s3->url($fileName, $this->bucket);
                }
            }
            if (!empty($_FILES) && $_FILES["event_banner"]["error"] != 4) {
                $files = $_FILES["event_banner"]["name"];
                $ext = $ext = pathinfo($files, PATHINFO_EXTENSION);
                $tmpName = $_FILES["event_banner"]["tmp_name"];
                $fileName = "baboo-cover/eventbanner_" . time() . ".$ext";
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $event_banner = $this->s3->url($fileName, $this->bucket);
                }
            }
            $data_insert = array(
                "event_name"=>$event_name,
                "event_start"=>$event_start,
                "event_end"=>$event_end,
                "event_image"=>$event_image,
                "event_banner"=>$event_banner,
                "event_redirect"=>$this->post("event_redirect"),
                "created_by"=>1,
            );
            $this->db->insert("events_baboo", $data_insert);
            if($this->db->insert_id()){
                $data_insert_cat = array(
                    "cat_book"=>$event_name,
                    "create_by"=>1
                );
                $this->db->insert("book_category", $data_insert_cat);
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = $http_response_header;
                $resp_message = "insert event success";
            }
        }else{
            $resp_message = "insert even failed because of empty data";
        }
        $responses = array(
            "code"=>$resp_code,
            "message"=>$resp_message,
            "data"=>$data_insert
        );
        $this->set_response($responses, $http_response_header);
    }

    public function updateEvent_post(){
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $data_update = array();
        $resp_code = $http_response_header;
        $resp_message = "update event failed";
        $event_name = $this->post("event_name");
        $event_start = (!empty($this->post("event_start"))) ? $this->post("event_start") : "";
        $event_end = (!empty($this->post("event_end"))) ? $this->post("event_end") : "";
        $this->db->select("event_name, event_start, event_end, event_image, event_banner");
        $gcacat = $this->db->get_where("events_baboo", array("event_id"=>$this->post("event_id")));
        if($gcacat && $gcacat->num_rows() > 0 && !empty($this->post("event_id"))){
            $event_banner = "";
            $event_image = "";
            if (!empty($_FILES) && $_FILES["event_image"]["error"] != 4) {
                if(!empty($gcacat->row()->event_image)) $this->s3->del($gcacat->row()->event_image, $this->bucket);
                $files = $_FILES["event_image"]["name"];
                $ext = $ext = pathinfo($files, PATHINFO_EXTENSION);
                $tmpName = $_FILES["event_image"]["tmp_name"];
                $fileName = "baboo-cover/eventimage_" . time() . ".$ext";
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $event_image = $this->s3->url($fileName, $this->bucket);
                }
            }
            if (!empty($_FILES) && $_FILES["event_banner"]["error"] != 4) {
                if(!empty($gcacat->row()->event_banner)) $this->s3->del($gcacat->row()->event_banner, $this->bucket);
                $files = $_FILES["event_banner"]["name"];
                $ext = $ext = pathinfo($files, PATHINFO_EXTENSION);
                $tmpName = $_FILES["event_banner"]["tmp_name"];
                $fileName = "baboo-cover/eventbanner_" . time() . ".$ext";
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $event_banner = $this->s3->url($fileName, $this->bucket);
                }
            }
            if(!empty($event_name)) $data_update["event_name"] = $event_name;
            if(!empty($event_start)) $data_update["event_start"] = $event_start;
            if(!empty($event_end)) $data_update["event_end"] = $event_end;
            if(!empty($event_image)) $data_update["event_image"] = $event_image;
            if(!empty($event_banner)) $data_update["event_banner"] = $event_banner;
            if(!empty($data_update)) {
                $upd = $this->db->update("events_baboo", $data_update, array("event_id"=>$this->post("event_id")));
                if($upd){
                    $data_insert_cat = array("cat_book"=>$event_name);
                    $this->db->select("id_catbook");
                    $gecat = $this->db->get_where("book_category", array("cat_book"=>$gcacat->row()->event_name));
                    if($gecat->num_rows() > 0) {
                        $this->db->update("book_category", $data_insert_cat, array("cat_book"=>$gcacat->row()->event_name));
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resp_code = $http_response_header;
                        $resp_message = "update event success";
                    }else{
                        $resp_code = $http_response_header;
                        $resp_message = "update event success but ".implode(",", $this->db->error())." last query".$this->db->last_query();

                    }
                }
            }
        }else{
            $resp_message = "update even failed because of empty data or not found event on database";
        }
        $responses = array(
            "code"=>$resp_code,
            "message"=>$resp_message,
            "data"=>$data_update
        );
        $this->set_response($responses, $http_response_header);
    }

    public function delEvent_post(){
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = $http_response_header;
        $event_id = (!empty($this->post("event_id"))) ? $this->post("event_id") : "";
        if(!empty($event_id)){
            $data_where = array(
                'event_id' => $event_id
            );
            $this->db->select("event_name");
            $event = $this->db->get_where("events_baboo", $data_where);
            if($event && $event->num_rows() > 0) {
                $event_name = (!empty($event->row()->event_name)) ? $event->row()->event_name : "";
                $data_where_cat = array(
                    "cat_book"=>$event_name
                );
                $upd = $this->db->update("book_category", array("stat_catbook"=>1), $data_where_cat);
                if($upd) {
                    $del = $this->db->delete("events_baboo", $data_where);
                    if($del && !empty($event_name)){
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resp_code = $http_response_header;
                        $resp_message = "delete event success";
                    }else{
                        $resp_code = $http_response_header;
                        $resp_message = "delete event success but ".implode(",", $this->db->error());
                    }
                }else{
                    $resp_code = $http_response_header;
                    $resp_message = "delete event success but ".implode(",", $this->db->error());
                }

            }else{
                $resp_code = $http_response_header;
                $resp_message = "delete event failed because ".implode(",", $this->db->error());
            }
        }else{
            $resp_message = "delete even failed because of empty data";
        }
        $responses = array(
            "code"=>$resp_code,
            "message"=>$resp_message
        );
        $this->set_response($responses, $http_response_header);
    }

    public function setEvent_post()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $email = $this->post("email");
        if (!empty($email)) {
            $this->db->where("email", $email);
            $user = $this->db->get("users");
            if ($user->num_rows() > 0) {
                $user_id = $user->row()->user_id;
                if (!empty($this->post("phone"))) {
                    $this->db->update("users", array("phone" => $this->post("phone")), array("user_id" => $user_id));
                }
                $event_id = $this->post("event_id");
                if (empty($event_id)) $where = "NOW() >= event_start AND NOW() <= event_end";
                else $where = "event_id = " . $event_id;
                $getEvent = $this->db->get_where("events_baboo", $where);
                if ($getEvent->num_rows() > 0) {
                    if (empty($event_id)) $event_id = $getEvent->row()->event_id;
                    $getDEvent = $this->db->get_where("events_detail", array("event_id" => $event_id, "user_id" => $user_id));
                    if ($getDEvent->num_rows() == 0) {
                        $ins = $this->db->insert("events_detail", array("event_id" => $event_id, "user_id" => $user_id));
                        if ($ins) {
                            $http_response_header = REST_Controller::HTTP_OK;
                            $resp_code = $http_response_header;
                            $resp_message = "registered for event success";
                        } else {
                            $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                            $this_err = $this->db->error();
                            $resp_message = "something went wrong with this apps " . implode(",", $this_err);
                        }
                    } else {
                        $resp_code = REST_Controller::HTTP_FORBIDDEN;
                        $resp_message = "you already registered for this event";
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_FORBIDDEN;
                    $resp_message = "event not existed";
                }

            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "user not registered yet, register first";
            }
        } else {
            $http_response_header = REST_Controller::HTTP_FORBIDDEN;
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "unauthorized, please contact admin";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        $this->set_response($responses, $http_response_header);
    }

    public function cekEvent_post()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $email = $this->post("email");
        if (!empty($email)) {
            $this->db->where("email", $email);
            $user = $this->db->get("users");
            if ($user->num_rows() > 0) {
                $user_id = $user->row()->user_id;
                $event_id = $this->post("event_id");
                if (empty($event_id)) $where = "NOW() >= event_start AND NOW() <= event_end";
                else $where = "event_id = " . $event_id;
                $getEvent = $this->db->get_where("events_baboo", $where);
                if ($getEvent->num_rows() > 0) {
                    if (empty($event_id)) $event_id = $getEvent->row()->event_id;
                    $getDEvent = $this->db->get_where("events_detail", array("event_id" => $event_id, "user_id" => $user_id));
                    if ($getDEvent->num_rows() == 0) {
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resp_code = $http_response_header;
                        $resp_message = "you can register for this event";
                    } else {
                        $resp_code = REST_Controller::HTTP_FORBIDDEN;
                        $resp_message = "you already registered for this event";
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "event not existed";
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "user not registered yet, register first";
            }
        } else {
            $http_response_header = REST_Controller::HTTP_FORBIDDEN;
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "unauthorized, please contact admin";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        $this->set_response($responses, $http_response_header);
    }
}