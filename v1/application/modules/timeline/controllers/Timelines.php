<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * Created by Abyan.
 * User: Abyan AF
 * Date: 15/11/2017
 * Time: 15:38
 */
class Timelines extends REST_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("baboo");
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //$this->methods['bestBook_get']['limit'] = 500; // 500 requests per hour per user/key
    }

    public function notifUpdatestat_post()
    {
        $bodyPost = (object)$this->post();
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = $http_response_header;
        $resp_message = "cannot update notif";
        if (!empty($this->post("notif_id"))) {
            $checks = $this->db->get_where("notifications", array("notif_id" => $bodyPost->notif_id, "status_notif !=" => -1));
            if ($checks->num_rows() > 0) {
                $status_notif = 1;
                if ($bodyPost->delete == true) {
                    $status_notif = -1;
                }
                $upd = $this->db->update("notifications", array("status_notif" => $status_notif), array("notif_id" => $checks->row()->notif_id));
                if ($upd) {
                    if ($status_notif > 0) $resp_message = "update notif success";
                    else $resp_message = "delete notif success";
                    $resp_code = REST_Controller::HTTP_OK;
                } else {
                    $thierr = "";
                    $thiserr = $this->db->error();
                    if (is_array($thiserr)) $thierr = $thiserr["code"] . " : " . $thiserr["message"];
                    $resp_message .= " because error on " . $thierr;
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "notif not found or has been deleted";
            }
        } else {
            $resp_message = "request for update not found";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        set_resp($responses, $http_response_header);
    }

    public function notification_post()
    {
        $limit = REST_Controller::limitNotif;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $this->db->limit($limit, $count);
        $this->db->order_by("notif_id", "desc");
        $data_where = "notif_to = " . $user_id . " AND notif_by NOT IN (SELECT notif_by FROM notifications WHERE notif_type != 4 AND notif_by = " . $user_id . ") AND status_notif != -1";
        $getNotif = $this->db->get_where("notifications", $data_where);
        $resp_data = array();
        if ($getNotif->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "notif set success";
            foreach ($getNotif->result() as $notval) {
                $this->db->select("user_id, fullname, prof_pict");
                $getusers = $this->db->get_where("users", array("user_id" => $notval->notif_by));
                $resp_users = array();
                $notif_userid = "";
                $notif_username = "";
                $notif_avatar = "";
                if ($getusers->num_rows() > 0) {
                    $notif_userid = $notval->notif_by;
                    $notif_username = $getusers->row()->fullname;
                    $notif_avatar = (string)$getusers->row()->prof_pict;
                }
                $resp_users["user_id"] = $notif_userid;
                $resp_users["fullname"] = $notif_username;
                $resp_users["prof_pict"] = (string)$notif_avatar;
                $resp_book = array();
                if (!empty($notval->notif_book)) {
                    $this->db->select("book_id, title_book, file_cover, category_id, is_pdf");
                    $getbook = $this->db->get_where("book", array("book_id" => $notval->notif_book));
                    if ($getbook->num_rows() > 0) {
                        $resp_book["book_id"] = $getbook->row()->book_id;
                        $resp_book["title_book"] = (string)$getbook->row()->title_book;
                        $resp_book["cover_url"] = (string)$getbook->row()->file_cover;
                        $resp_book["is_pdf"] = (!empty($getbook->row()->is_pdf)) ? false : true;
                        $this->db->where("id_catbook", $getbook->row()->category_id);
                        $this->db->select("id_catbook as category_id, cat_book as category_name");
                        $gcatbook = $this->db->get("book_category");
                        $catbooks = (object)array();
                        if ($gcatbook->num_rows() > 0) {
                            $catbooks = $gcatbook->row();
                        }
                        $resp_book["book_genre"] = $catbooks;
                    }
                }
                $notif_desc = "";
                $id_notype = $notval->notif_type;
                $this->db->select("notif_types");
                $notf = $this->db->get_where("notif_type", array("notype_id" => $id_notype));
                if ($notf->num_rows() > 0) {
                    $notif_desc = $notf->row()->notif_types;
                }
                switch ($notval->status_notif) {
                    case 0  :
                        $notif_status = "unread";
                        break;
                    case 1  :
                        $notif_status = "read";
                        break;
                    default :
                        $notif_status = "unread";
                        break;
                }
                $resp_data[] = array(
                    "notif_id" => $notval->notif_id,
                    "notif_text" => $notval->notif_text,
                    "notif_time" => time_ago($notval->notif_date),
                    "notif_status" => $notif_status,
                    "notif_type" => array(
                        "notif_type_id" => $notval->notif_type,
                        "notif_type_desc" => $notif_desc
                    ),
                    "notif_user" => (object)$resp_users,
                    "notif_book" => (object)$resp_book,
                );

            }
        } else {
            $resp_message = "notif not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
    }

    public function likeBook_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $book_id = $this->post("book_id");
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        if (!empty($user_id) && !empty($book_id)) {
            $rest_case = "";
            $data_insert = array("id_user" => $user_id, "id_book" => $book_id);
            $ceklike = $this->db->get_where("likes", $data_insert);
            if ($ceklike->num_rows() == 0) {
                $xq = $this->db->insert("likes", $data_insert);
                if ($xq) {
                    $rest_case = "add like";
                    $this->db->query("UPDATE book SET like_count = like_count + 1 WHERE book_id = ?", array($book_id));
                    $this->db->where("book_id", $this->post("book_id"));
                    $this->db->select("book_id, is_pdf, author_id, title_book");
                    $gbook = $this->db->get("book");
                    if ($gbook->num_rows() > 0) {
                        $book_id = $this->post("book_id");
                        $author_id = $gbook->row()->author_id;
                        $is_pdf = $gbook->row()->is_pdf;
                        if ($author_id != $user_id) {
                            $this->db->select("user_id, email, fullname");
                            $users = $this->db->get_where("users", array("user_id" => $user_id));
                            if ($users->num_rows() > 0) {
                                $getLike = $this->db->get_where("likes", "id_book = " . $book_id . " AND id_user != " . $user_id);
                                $jmlike = $getLike->num_rows();
                                $name = "";
                                if ($jmlike > 0) $name .= " dan " . $jmlike . " orang lainnya";
                                $notif_text = $name . " menyukai tulisan anda";
                                $arr_notif = array(
                                    "notif_type" => REST_Controller::notif_like,
                                    "notif_to" => $author_id,
                                    "notif_book" => $book_id,
                                    "notif_by" => $user_id,
                                    "notif_text" => $notif_text
                                );
                                $this->db->insert("notifications", $arr_notif);
                                $insid = $this->db->insert_id();
                                if ($insid) {
                                    $notif_text = $users->row()->fullname . $notif_text;
                                    $type_book = (!empty($is_pdf)) ? REST_Controller::type_book : REST_Controller::type_pdf;
                                    setpush_notif($author_id, $notif_text, $type_book, $book_id);
                                }
                            }
                        }
                    }
                }
            } else {
                $xq = $this->db->delete("likes", $data_insert);
                if ($xq) {
                    $rest_case = "unlike";
                    $this->db->query("UPDATE book SET like_count = like_count - 1 WHERE book_id = ?", array($book_id));
                }
            }
            //checking if query executed
            if (!empty($rest_case)) {
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = $rest_case . " success";
            } else {
                $array_error = $this->db->error();
                if (!empty($array_error)) {
                    $resp_code = $array_error["code"];
                    $resp_message = $array_error["message"];
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "user and book cannot be found";
                }
            }
        } else {
            $resp_code = REST_Controller::HTTP_BAD_REQUEST;
            $resp_message = "user or book cannot be empty";
        }
        $responses = array("code" => $resp_code, "message" => $resp_message, "data" => (object)array());
        set_resp($responses, $http_response_header);
    }

    public function message_post()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $message = $this->post("message");
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $user_from = $user_id;
        $user_to = $this->post("user_to");
        $resp_code = REST_Controller::HTTP_BAD_REQUEST;
        $resp_message = "message sent";
        $resp_data = array();
        if (!empty($user_to) && !empty($user_from)) {
            $cmd = "SELECT message_id FROM messages WHERE (created_by = ? OR user_to = ?) AND (created_by = ? OR user_to = ?) AND reply_to = 0 ORDER BY created_at DESC";
            $reply_to = 0;
            $gmes = $this->db->query($cmd, array($user_from, $user_from, $user_to, $user_to));
            if ($gmes->num_rows() > 0) {
                $reply_to = $gmes->row()->message_id;
            }
            $arr_message = array();
            $arr_message["user_from"] = $user_from;
            $arr_message["created_by"] = $user_from;
            $arr_message["user_to"] = $user_to;
            $arr_message["message"] = $message;
            $arr_message["reply_to"] = $reply_to;
            $times = date("Y-m-d H:i:s");
            $this->db->insert("messages", $arr_message);
            $insid = $this->db->insert_id();
            if ($insid) {
                $fullname = "";
                $prof_pict = "";
                $users = $this->db->get_where("users", array("user_id" => $user_from));
                if ($users->num_rows() > 0) {
                    $fullname = $users->row()->fullname;
                    $prof_pict = $users->row()->prof_pict;
                }
                $resp_code = REST_Controller::HTTP_OK;
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_data = array(
                    "user_id" => $user_from,
                    "fullname" => $fullname,
                    "prof_pict" => (string)$prof_pict,
                    "message" => $message,
                    "chat_time" => time_ago($times)
                );
                setpush_notif($user_to, $message, REST_Controller::type_message, $user_from);
            } else {
                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $thiserr = $this->db->error();
                if (is_array($thiserr)) $thiserr = $thiserr["code"] . " : " . $thiserr["message"];
                $resp_message = "message failed to sent, because " . $thiserr;
            }
        } else {
            $resp_message = "No user selected to send message";
        }

        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data,
        );
        set_resp($resps, $http_response_header);
    }

    public function listMessage_post()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_BAD_REQUEST;
        $resp_message = "show message success";
        $resp_data = array();
        if (!empty($user_id)) {
            $gmes = $this->db->query("SELECT message_id, user_from, user_to, message, created_by, created_at FROM messages WHERE (user_from = $user_id OR user_to = $user_id) AND reply_to = 0 ORDER BY created_at DESC");
            if ($gmes->num_rows() > 0) {
                foreach ($gmes->result() as $gmval) {
                    $lmes = $this->db->query("SELECT message_id, user_from, user_to, message, created_by, created_at FROM messages WHERE (user_from = $user_id OR user_to = $user_id) AND reply_to = " . $gmval->message_id . " ORDER BY created_at DESC");
                    if ($lmes->num_rows() > 0) {
                        $lmesx = $lmes->row();
                    } else {
                        $lmesx = $gmval;
                    }
                    $times = $lmesx->created_at;
                    $user_with = $lmesx->user_to;
                    if ($user_with == $user_id) {
                        $user_with = $lmesx->user_from;
                    }
                    if ($user_with != $user_id) {
                        $gudb = $this->db->get_where("users", array("user_id" => $user_with));
                        if ($gudb->num_rows() > 0) {
                            $userwith_name = $gudb->row()->fullname;
                            $userwith_avatar = (string)$gudb->row()->prof_pict;
                        }
                    }
                    $latests = "";
                    $latest_user = $lmesx->created_by;
                    $ldb = $this->db->get_where("users", array("user_id" => $latest_user));
                    if ($ldb->num_rows() > 0) $latests = $ldb->row()->fullname;

                    $resp_code = REST_Controller::HTTP_OK;
                    $http_response_header = REST_Controller::HTTP_OK;
                    $latests_message = $lmesx->message;
                    $resp_data[] = array(
                        "user_id" => $user_with,
                        "fullname" => $userwith_name,
                        "prof_pict" => $userwith_avatar,
                        "user_latest_message" => $latests . " : " . $latests_message,
                        "user_chat_time" => time_ago($times)
                    );
                }
            } else {
                $thiserr = $this->db->error();
                if (is_array($thiserr)) {
                    if (!empty($thiserr["code"])) {
                        $resp_code = REST_Controller::HTTP_BAD_REQUEST;
                        $resp_message = "mesage error because" . $thiserr["code"] . " : " . $thiserr["message"];
                    } else {
                        $resp_code = REST_Controller::HTTP_OK;
                        $resp_message = "message empty";
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_message = "message empty, start new message";
                }

            }
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "no user selected to send message";
        }
        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (array)$resp_data,
        );
        set_resp($resps, $http_response_header);
    }

    public function detailMessage_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $user_with = (int)$this->post("user_with");
        $resp_code = REST_Controller::HTTP_BAD_REQUEST;
        $resp_message = "show message success";
        $resp_data = array();
        if (!empty($user_id) && !empty($user_with)) {
            if ($user_id != $user_with) {
                $gudb = $this->db->get_where("users", array("user_id" => $user_with));
                if ($gudb->num_rows() > 0) {
                    $userwith_name = $gudb->row()->fullname;
                    $userwith_avatar = (string)$gudb->row()->prof_pict;
                }
                $resp_data["user_with"] = array(
                    "user_id" => $user_with,
                    "fullname" => $userwith_name,
                    "prof_pict" => $userwith_avatar
                );
            }
            $gmes = $this->db->query("SELECT user_id, fullname, user_to, `message`, IFNULL(prof_pict, '') AS prof_pict, created_at FROM users INNER JOIN messages ON users.user_id = messages.user_from WHERE (user_id = ? OR user_to = ?) AND (user_id = ? OR user_to = ?) ORDER BY created_at ASC", array($user_id, $user_id, $user_with, $user_with));
            if ($gmes->num_rows() > 0) {
                $arr_message = array();
                foreach ($gmes->result() as $gmval) {
                    $lmesx = $gmval;
                    $times = $lmesx->created_at;
                    $resp_code = REST_Controller::HTTP_OK;
                    $http_response_header = REST_Controller::HTTP_OK;
                    $arr_message[] = array(
                        "user_id" => $lmesx->user_id,
                        "fullname" => $lmesx->fullname,
                        "prof_pict" => $lmesx->prof_pict,
                        "message" => $lmesx->message,
                        "chat_time" => time_ago($times)
                    );
                }
                $resp_data["messages"] = $arr_message;
            } else {
                $resp_data["messages"] = array();
                $thiserr = $this->db->error();
                if (is_array($thiserr)) {
                    if (!empty($thiserr["code"])) {
                        $resp_code = REST_Controller::HTTP_BAD_REQUEST;
                        $resp_message = "mesage error because" . $thiserr["code"] . " : " . $thiserr["message"];
                    } else {
                        $resp_code = REST_Controller::HTTP_OK;
                        $resp_message = "message empty";
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_message = "message empty, start new message";
                }
            }
        } else {
            $resp_message = "No user selected to send message > ";
        }

        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data,
        );
        set_resp($resps, $http_response_header);
    }

    public function follow_post()
    {
        $data = array();
        $http_response_header = REST_Controller::HTTP_OK;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $data["is_follow"] = $this->post("user_follow");
        $data["created_by"] = $user_id;
        $resp_message = "";
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        if (!empty($this->post("user_follow"))) {
            $rest_case = "";
            $data_insert = $data;
            $ceklike = $this->db->get_where("follows", $data_insert);
            if ($ceklike->num_rows() == 0) {
                $xq = $this->db->insert("follows", $data_insert);
                $rest_case = "followed";
                if ($xq) {
                    $users = $this->db->get_where("users", array("user_id" => $user_id));
                    if ($users->num_rows() > 0) {
                        $notif_text = "mulai mengikuti anda";
                        $arr_notif = array(
                            "notif_type" => 1,
                            "notif_to" => $this->post("user_follow"),
                            "notif_by" => $user_id,
                            "notif_text" => $notif_text
                        );
                        $this->db->insert("notifications", $arr_notif);
                        $insd = $this->db->insert_id();
                        if ($insd) {
                            $notifs_text = $users->row()->fullname . " " . $notif_text;
                            setpush_notif($this->post("user_follow"), $notifs_text, REST_Controller::type_user, $user_id);
                        }
                    }
                }
            } else {
                $xq = $this->db->delete("follows", $data_insert);
                $rest_case = "unfollowed";
            }
            //checking if query executed
            if (!empty($rest_case)) {
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = $rest_case;
            } else {
                $array_error = $this->db->error();
                if (!empty($array_error)) {
                    $resp_code = $array_error["code"];
                    $resp_message = $array_error["message"];
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "user cannot be found or followed";
                }
            }
        } else {
            $resp_code = REST_Controller::HTTP_BAD_REQUEST;
            $resp_message = "user cannot be empty";
        }
        $resps = array("code" => $resp_code, "message" => $resp_message, "data" => (object)array());
        set_resp($resps, $http_response_header);
    }

    public function draft_post()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $limit = REST_Controller::limitDraftPublish;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_message = "show draft book success";
        $resp_data = array();
        $this->db->limit($limit, $count);
        // $this->db->order_by("book.share_count", "desc");
        $this->db->order_by("book.book_id", "desc");
        $this->db->where("book.status_publish !=", "publish");
        $this->db->where("book.author_id", $user_id);
        $this->db->join("users", "users.user_id = book.author_id");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books->num_rows() > 0) {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_OK;
            $http_response_header = $resp_code;
            $limit_text = REST_Controller::text_limits;
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                $file_cover = "";
                if(!empty($boval->is_pdf)) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = (string)$boval->descriptions;
                $latest_update = $boval->latest_update;
                if (empty($latest_update)) $latest_update = $boval->created_date;
                if (!empty($boval->title_book)) {
                    if (!empty($boval->publish_at)) $publish_at = $boval->publish_at;
                    $stat_book = $boval->status_publish;
                    $stat_publish = setStats($stat_book);
                    $publish_date = (!empty($boval->publish_at)) ? $boval->publish_at : "";
                    $getComment = $this->db->get_where("comment", array("id_book" => $boval->book_id));
                    $category = getCategoryBook($boval->category_id);
                    //check if image url exist
                    if (!empty($boval->file_cover)) $file_cover = $boval->file_cover;
                    $is_download = false;
                    if (!empty($boval->image_url)) $image_url = $boval->image_url;
                    else $image_url = $file_cover;

                    $dasray = array(
                        "book_id" => $boval->book_id,
                        "author_id" => $boval->author_id,
                        "author_name" => $boval->fullname,
                        "author_avatar" => (string)$boval->prof_pict,
                        "cover_url" => (string)$file_cover,
                        "category" => $category,
                        "latest_update" => time_ago($latest_update),
                        "is_like" => false,
                        "is_bookmark" => false,
                        "is_download" => $is_download,
                        "book_type" => (int)$boval->book_type,
                        "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                        "publish_date" => $publish_date,
                        "status_publish" => $stat_publish,
                        "image_url" => (string)$image_url,
                        "title_book" => (string)$boval->title_book,
                        "view_count" => (int)$boval->view_count,
                        "share_count" => (int)$boval->share_count,
                        "like_count" => (int)$boval->like_count,
                        "comment_count" => (int)$getComment->num_rows(),
                        "desc" => $res_par_text
                    );

                    $resp_data[] = $dasray;
                }
            }
        } else {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = 'no book data';
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, $http_response_header);
    }

    public function listCollections_get()
    {
        $limit = REST_Controller::limitCollection;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_message = "";
        $http_code_resp = REST_Controller::HTTP_OK;
        $resp_data = array();
        $this->db->limit($limit, $count);
        $this->db->order_by("collections.collections_id", "desc");
        $this->db->where("book.status_publish !=", "draft");
        $this->db->where("collections.id_user", $user_id);
        $this->db->join("collections", "collections.id_book = book.book_id");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books->num_rows() > 0) {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_OK;
            $http_response_header = $resp_code;
            $limit_text = REST_Controller::text_limits;
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                $dasray = array();
                $is_likes = checkIsLike($boval->book_id, $user_id);
                $is_bookmark = checkIsBookmark($boval->book_id, $user_id);
                if(!empty($boval->is_pdf)) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = (string)$boval->descriptions;
                $is_download = false;
                if (isset($boval->is_download) && $boval->is_download == 1) $is_download = true;
                $getComment = $this->db->get_where("comment", array("id_book" => $boval->book_id));
                $category = getCategoryBook($boval->category_id);
                $latest_update = "";
                $fullname = "";
                $prof_pict = "";
                $users = getUsersById($boval->author_id);
                if(!empty($users)){
                    $fullname = $users["fullname"];
                    $prof_pict = $users["prof_pict"];
                }
                if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                if (!empty($res_par_text) && !empty($boval->title_book)) {
                    $dasray = array(
                        "book_id" => $boval->book_id,
                        "author_id" => $boval->author_id,
                        "author_name" => $fullname,
                        "author_avatar" => (string)$prof_pict,
                        "cover_url" => (string)$boval->file_cover,
                        "image_url" => (string)$boval->image_url,
                        "title_book" => $boval->title_book,
                        "category" => $category,
                        "is_like" => $is_likes,
                        "is_bookmark" => $is_bookmark,
                        "book_type" => (int)$boval->book_type,
                        "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                        "is_bought" => true,
                        "is_download" => $is_download,
                        "latest_update" => $latest_update,
                        "publish_date" => time_ago($boval->publish_at),
                        "like_count" => (int)$boval->like_count,
                        "comment_count" => (int)$getComment->num_rows(),
                        "view_count" => (int)$boval->view_count,
                        "share_count" => (int)$boval->share_count,
                        "desc" => trim($res_par_text)
                    );

                    $resp_data[] = $dasray;
                }
            }
        } else {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = 'no collections data';
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, $http_code_resp);
    }

    public function allCollections_get()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_message = "";
        $http_code_resp = REST_Controller::HTTP_OK;
        $resp_data = array();
        $this->db->order_by("collections.collections_id", "desc");
        $this->db->where("book.status_publish !=", "draft");
        $this->db->where("collections.id_user", $user_id);
        $this->db->join("collections", "collections.id_book = book.book_id");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books->num_rows() > 0) {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_OK;
            $http_response_header = $resp_code;
            $limit_text = REST_Controller::text_limits;
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                $dasray = array();
                $is_likes = checkIsLike($boval->book_id, $user_id);
                $is_bookmark = checkIsBookmark($boval->book_id, $user_id);
                if(!empty($boval->is_pdf)) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = (string)$boval->descriptions;
                $is_download = false;
                if (isset($boval->is_download) && $boval->is_download == 1) $is_download = true;
                $getComment = $this->db->get_where("comment", array("id_book" => $boval->book_id));
                $category = getCategoryBook($boval->category_id);
                $latest_update = "";
                $fullname = "";
                $prof_pict = "";
                $users = getUsersById($boval->author_id);
                if(!empty($users)){
                    $fullname = $users["fullname"];
                    $prof_pict = $users["prof_pict"];
                }
                if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                if (!empty($res_par_text) && !empty($boval->title_book)) {
                    $dasray = array(
                        "book_id" => $boval->book_id,
                        "author_id" => $boval->author_id,
                        "author_name" => $fullname,
                        "author_avatar" => (string)$prof_pict,
                        "cover_url" => (string)$boval->file_cover,
                        "image_url" => (string)$boval->image_url,
                        "title_book" => $boval->title_book,
                        "category" => $category,
                        "is_like" => $is_likes,
                        "is_bookmark" => $is_bookmark,
                        "book_type" => (int)$boval->book_type,
                        "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                        "is_bought" => true,
                        "is_download" => $is_download,
                        "latest_update" => $latest_update,
                        "publish_date" => time_ago($boval->publish_at),
                        "like_count" => (int)$boval->like_count,
                        "comment_count" => (int)$getComment->num_rows(),
                        "view_count" => (int)$boval->view_count,
                        "share_count" => (int)$boval->share_count,
                        "desc" => trim($res_par_text)
                    );

                    $resp_data[] = $dasray;
                }
            }
        } else {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = 'no collections data';
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, $http_code_resp);
    }

    public function collections_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $book_id = $this->post("book_id");
        $resp_message = "";
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        if (!empty($user_id) && !empty($book_id)) {
            $data_insert = array("id_user" => $user_id, "id_book" => $book_id);
            $ins = $this->db->insert("collections", $data_insert);
            //checking if query executed
            if ($ins) {
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "add to collections success";
            } else {
                $array_error = $this->db->error();
                if (!empty($array_error)) {
                    $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    $resp_message = "fail to add collections because x" . $array_error["code"] . " : " . $array_error["message"];
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "book not found";
                }
            }
        } else {
            $resp_code = REST_Controller::HTTP_BAD_REQUEST;
            $resp_message = "user or book cannot be empty";
        }
        set_resp(array("code" => $resp_code, "message" => $resp_message, "data" => (object)array()), $http_response_header);
    }

    public function listBookmark_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $limit = REST_Controller::limitBookmark;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_message = "";
        $resp_data = array();
        $this->db->limit($limit, $count);
        $this->db->order_by("bookmark.bookmark_id", "desc");
//        $this->db->order_by("book.share_count", "desc");
//        $this->db->order_by("book.view_count", "desc");
        $this->db->where("book.status_publish !=", "draft");
        $this->db->where("bookmark.id_user", $user_id);
        $this->db->join("bookmark", "bookmark.id_book = book.book_id");
        $this->db->join("users", "book.author_id = users.user_id");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books->num_rows() > 0) {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_OK;
            $limit_text = REST_Controller::text_limits;
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                if(!empty($boval->is_pdf)) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = (string)$boval->descriptions;
                $dasray = array();
                $is_likes = checkIsLike($boval->book_id, $user_id);
                if (!empty($res_par_text) && !empty($boval->title_book)) {
                    $is_download = false;
                    $this->db->select("is_download");
                    $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $boval->book_id));
                    if ($cekBought->num_rows() > 0) {
                        if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                    }
                    $category = getCategoryBook($boval->book_id);
                    $dasray = array(
                        "book_id" => $boval->book_id,
                        "author_id" => $boval->author_id,
                        "author_name" => $boval->fullname,
                        "author_avatar" => (string)$boval->prof_pict,
                        "category" => (string)$category,
                        "is_like" => $is_likes,
                        "is_download" => $is_download,
                        "book_type" => (int)$boval->book_type,
                        "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                        "cover_url" => (string)$boval->file_cover,
                        "image_url" => (string)$boval->image_url,
                        "title_book" => $boval->title_book,
                        "desc" => trim($res_par_text)
                    );
                    $resp_data[] = $dasray;
                }
            }
        } else {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = 'no bookmark data';
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, $http_response_header);
    }

    public function allBookmark_get()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_message = "";
        $resp_data = array();
        $this->db->order_by("bookmark.bookmark_id", "desc");
        $this->db->where("book.status_publish !=", "draft");
        $this->db->where("bookmark.id_user", $user_id);
        $this->db->join("bookmark", "bookmark.id_book = book.book_id");
        $this->db->join("users", "book.author_id = users.user_id");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books->num_rows() > 0) {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_OK;
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                if(!empty($boval->is_pdf)) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = (string)$boval->descriptions;
                $dasray = array();
                $is_likes = checkIsLike($boval->book_id, $user_id);
                if (!empty($res_par_text) && !empty($boval->title_book)) {
                    $is_download = false;
                    $this->db->select("is_download");
                    $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $boval->book_id));
                    if ($cekBought->num_rows() > 0) {
                        if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                    }
                    $category = getCategoryBook($boval->book_id);
                    $dasray = array(
                        "book_id" => $boval->book_id,
                        "author_id" => $boval->author_id,
                        "author_name" => $boval->fullname,
                        "author_avatar" => (string)$boval->prof_pict,
                        "category" => (string)$category,
                        "is_like" => $is_likes,
                        "is_download" => $is_download,
                        "book_type" => (int)$boval->book_type,
                        "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                        "cover_url" => (string)$boval->file_cover,
                        "image_url" => (string)$boval->image_url,
                        "title_book" => $boval->title_book,
                        "desc" => trim($res_par_text)
                    );
                    $resp_data[] = $dasray;
                }
            }
        } else {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = 'no bookmark data';
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, $http_response_header);
    }

    public function publish_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $limit = REST_Controller::limitDraftPublish;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_message = "show publish book success";
        $resp_data = array();
        $this->db->limit($limit, $count);
//        $this->db->order_by("book.share_count", "desc");
        $this->db->order_by("book.book_id", "desc");
        $this->db->where("book.status_publish != 'draft'");
        $this->db->where("book.author_id", $user_id);
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->select("book_id, title_book, author_id, fullname, prof_pict, category_id, book_type, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books->num_rows() > 0) {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_OK;
            $limit_text = REST_Controller::text_limits;
            $last_query = $this->db->last_query();
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                if(!empty($boval->is_pdf)) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = substr((string)$boval->descriptions, 0, $limit_text) . "...";
                $dasray = array();
                $is_likes = checkIsLike($boval->book_id, $user_id);
                $is_bookmark = checkIsBookmark($boval->book_id, $user_id);
                $publish_date = time_ago($boval->publish_at);
                $latest_update = $publish_date;
                if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                if (!empty($boval->title_book)) {
                    $getComment = $this->db->get_where("comment", array("id_book" => $boval->book_id));
                    $category = getCategoryBook($boval->category_id);
                    $is_download = false;
                    $this->db->select("is_download");
                    $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $boval->book_id));
                    if ($cekBought->num_rows() > 0) {
                        if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                    }
                    $dasray = array(
                        "book_id" => $boval->book_id,
                        "author_id" => $boval->author_id,
                        "author_name" => $boval->fullname,
                        "author_avatar" => (string)$boval->prof_pict,
                        "cover_url" => (string)$boval->file_cover,
                        "image_url" => (string)$boval->image_url,
                        "title_book" => $boval->title_book,
                        "category" => $category,
                        "is_like" => $is_likes,
                        "is_bookmark" => $is_bookmark,
                        "is_download" => $is_download,
                        "book_type" => (int)$boval->book_type,
                        "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                        "latest_update" => $latest_update,
                        "publish_date" => time_ago($boval->publish_at),
                        "like_count" => (int)$boval->like_count,
                        "comment_count" => (int)$getComment->num_rows(),
                        "view_count" => (int)$boval->view_count,
                        "share_count" => (int)$boval->share_count,
                        "desc" => $res_par_text
                    );
                    $resp_data[] = $dasray;
                }
            }
        } else {
            // Set the response and exit
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = 'no book data';
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, $http_response_header);
    }

    public function search_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $search_results = array();
        $body = (object)$this->post();
        $resp_message = "";
        $limit = REST_Controller::limitsearchs;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        $this->db->limit($limit, $count);
        $this->db->where("fullname LIKE '%" . $body->search . "%'");
        $this->db->select("user_id, role_id, fullname, email, IFNULL(date_of_birth, '') as date_of_birth,  IFNULL(jk, '') as jk, IFNULL(prof_pict, '') as prof_pict, IFNULL(address, '') as address,  IFNULL(about_me, '') as about_me, book_sold, balance, IFNULL(phone, '') as phone, oauth_uid, oauth_provider");
        $susr = $this->db->get("users");
        $sarus = array();
        if ($susr->num_rows() > 0) {
            foreach ($susr->result() as $sasrow) {
                $arus = array();
                $is_follow = false;
                $this->db->select("created_by");
                $cekfollow = $this->db->get_where("follows", array("created_by" => $user_id, "is_follow" => $sasrow->user_id));
                if ($cekfollow->num_rows() > 0) $is_follow = true;
                $followers = $this->db->query("SELECT COUNT(created_by) as followers FROM follows WHERE is_follow = ?", array($sasrow->user_id));
                $book_made = $this->db->query("SELECT COUNT(book_id) as book_made FROM book WHERE author_id = ? AND status_publish !=  'draft'", array($sasrow->user_id));
                $arus["user_id"] = $sasrow->user_id;
                $arus["role_id"] = $sasrow->role_id;
                $arus["fullname"] = (string)$sasrow->fullname;
                $arus["email"] = $sasrow->email;
                $arus["prof_pict"] = (string)$sasrow->prof_pict;
                $arus["date_of_birth"] = (string)$sasrow->date_of_birth;
                $arus["jk"] = (string)$sasrow->jk;
                $arus["address"] = (string)$sasrow->address;
                $arus["about_me"] = (string)$sasrow->about_me;
                $arus["oauth_uid"] = (string)$sasrow->oauth_uid;
                $arus["oauth_provider"] = (string)$sasrow->oauth_provider;
                $arus["followers"] = ($followers && $followers->num_rows() > 0) ? (int)$followers->row()->followers : 0;
                $arus["book_sold"] = (int)$sasrow->book_sold;
                $arus["book_made"] = ($book_made && $book_made->num_rows() > 0) ? (int)$book_made->row()->book_made : 0;
                $arus["balance"] = (int)$sasrow->balance;
                $arus["is_newuser"] = false;
                $arus["isFollow"] = $is_follow;
                $arus["has_pin"] = false;
                $arus["is_activated"] = false;
                $sarus[] = $arus;
            }
        }
        $search_results["user"] = $sarus;

        $sabook = array();
        $where = "title_book LIKE '%" . $body->search . "%'";
        $books = (array)$this->setBook($where, 0, $user_id);
        $count_book = 0;
        if (is_array($books) && count($books) > 0) {
            $count_book = count($books);
            $sabook = json_decode(json_encode($books), true);
        }
        if (!empty($sabook)) $search_results["books"] = $sabook;
        else $search_results["books"] = array();

        if (isset($search_results["books"]) && count($search_results["books"]) > 0) $search_results["books"] = array_slice($search_results["books"], 0, 3);
//        $search_results["books"] = search_results["books"];
        if (count($search_results) > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps["code"] = REST_Controller::HTTP_OK;
            $resps["message"] = $resp_message;
            $resps["data"] = (array)$search_results;
        } else {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps["code"] = REST_Controller::HTTP_OK;
            $resps["message"] = "search result not found";
            $resps["data"] = (array)$search_results;
        }
        set_resp($resps, $http_response_header);
    }

    public function searchCategory_post()
    {
        $search_results = array();
        $body = (object)$this->post();
        $this->db->select("user_id, fullname, email, IFNULL(prof_pict, '') as prof_pict");
        $this->db->where("user_id IN (SELECT author_id FROM book WHERE category_id = " . $body->category_id . ")");
        $susr = $this->db->get("users");
        if ($susr->num_rows() > 0) {
            $sarus = array();
            foreach ($susr->result() as $sasrow) {
                $arus = array();
                $is_follow = false;
                $cekfollow = $this->db->get_where("follows", array("created_by" => $body->user_id, "is_follow" => $sasrow->user_id));
                if ($cekfollow->num_rows() > 0) $is_follow = true;
                $arus["user_id"] = $sasrow->user_id;
                $arus["fullname"] = (string)$sasrow->user_name;
                $arus["email"] = $sasrow->email;
                $arus["prof_pict"] = (string)$sasrow->prof_pict;
                $arus["isFollow"] = $is_follow;
                $sarus[] = $arus;
            }
            $search_results["user"] = $sarus;
        }
        $where = "category_id = " . $body->category_id;
        $books = (array)$this->setBook($where);
        if (is_array($books) && count($books) > 0) {
            $search_results["books"] = json_decode(json_encode($books), true);
        }
        if (count($search_results) > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps["code"] = REST_Controller::HTTP_OK;
            $resps["message"] = "";
            $resps["data"] = (array)$search_results;
        } else {
            $http_response_header = REST_Controller::HTTP_ACCEPTED;
            $resps["code"] = REST_Controller::HTTP_NOT_FOUND;
            $resps["message"] = "search result not found";
            $resps["data"] = (array)$search_results;
        }
        set_resp($resps, $http_response_header);
    }

    private function setBook($whereBook, $key = 0, $user_id = '')
    {
        $searchs = array();
        $this->db->order_by("book.publish_at", "desc");
        $this->db->where("status_publish !=", "draft");
        $this->db->where($whereBook);
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->select("book_id, title_book, author_id, fullname, prof_pict, category_id, book_type, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions");
        $gebok = $this->db->get("book");
        if ($gebok->num_rows() > 0) {
            foreach ($gebok->result() as $gkey => $gebov) {
                $key_array = $key + $gkey;
                $saruas = array();
                $is_bookmark = false;
                $is_likes = false;
                $is_download = false;
                $getComment = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($gebov->book_id));
                $category = getCategoryBook($gebov->category_id);
                $saruas["book_id"] = $gebov->book_id;
                $saruas["title_book"] = (string)$gebov->title_book;
                $saruas["author_id"] = $gebov->author_id;
                $saruas["author_name"] = (string)$gebov->fullname;
                $saruas["author_avatar"] = (string)$gebov->prof_pict;
                $saruas["cover_url"] = (string)$gebov->file_cover;
                $saruas["image_url"] = (string)$gebov->image_url;
                $latest_update = "";
                if (!empty($gebov->latest_update) && $gebov->latest_update != $gebov->publish_at && $gebov->status_publish == "publish") $latest_update = time_ago($gebov->latest_update);
                $saruas["latest_update"] = $latest_update;
                $saruas["publish_date"] = time_ago($gebov->publish_at);
                $saruas["category"] = (string)$category;
                if (!empty($user_id)) {
                    $this->db->select("id_book, id_user");
                    $getlikes = $this->db->get_where("likes", array("id_book" => $gebov->book_id, "id_user" => $user_id));
                    if ($getlikes->num_rows() > 0) {
                        $is_likes = true;
                    }
                    $this->db->select("id_book, id_user");
                    $getbookmark = $this->db->get_where("bookmark", array("id_book" => $gebov->book_id, "id_user" => $user_id));
                    if ($getbookmark->num_rows() > 0) {
                        $is_bookmark = true;
                    }
                    $this->db->select("is_download");
                    $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $gebov->book_id));
                    if ($cekBought->num_rows() > 0) {
                        if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                    }
                }
                $saruas["is_like"] = $is_likes;
                $saruas["is_bookmark"] = $is_bookmark;
                $saruas["is_download"] = $is_download;
                $saruas["book_type"] = (int)$gebov->book_type;
                $saruas["is_pdf"] = (!empty($gebov->is_pdf)) ? false : true;
                $saruas["like_count"] = (int)$gebov->like_count;
                $saruas["comment_count"] = ($getComment && $getComment->num_rows() > 0) ? (int)$getComment->row()->comment_count : 0;
                $saruas["share_count"] = (int)$gebov->share_count;
                $saruas["view_count"] = (int)$gebov->view_count;
                if(!empty($gebov->is_pdf)) $res_par_text = getParagraphByBook($gebov->book_id);
                else $res_par_text = substr((string)$gebov->descriptions, 0, REST_Controller::text_limits) . "...";
                $saruas["desc"] = $res_par_text;
                $searchs[] = $saruas;
            }
        }
        return (array)$searchs;
    }

    public function getComment_post()
    {
        $data = array();
        $resp_data = array();
        $paragraphs = "";
        $wheres = "";
        $keyes = "";
        $http_response_header = REST_Controller::HTTP_OK;
        if (!empty($this->post("paragraph_id"))) {
            $wheres = "WHERE id_paragraph = ?";
            $keyes = $this->post("paragraph_id");
            $data["id_paragraph"] = $this->post("paragraph_id");
            $gpar = $this->db->get_where("paragraph", array("paragraph_id" => $this->post("paragraph_id")));
            if ($gpar->num_rows() > 0) {
                $paragraphs = $gpar->row()->txt_paragraph;
                preg_match_all("/<video[^>]*>(.*?)<\/video>/is", $gpar->row()->txt_paragraph, $matchev);
                preg_match_all("/<img[^>]*\/>/is", $gpar->row()->txt_paragraph, $matchevas);
                if (count($matchev) == 0 && count($matchevas) == 0) {
                    $paragraphs = $this->strip_tags_with_whitespace($paragraphs);
                    $paragraphs = substr($paragraphs, 0, REST_Controller::limitParComment);
                }
            }
        }
        $radata = array();
        $resp_data["paragraph"] = $paragraphs;
        if (!empty($this->post("book_id"))) {
            $data["id_book"] = $this->post("book_id");
            $wheres = "WHERE id_book = ?";
            $keyes = $this->post("book_id");
        }

        if (!empty($this->post("paragraph_id")) || !empty($this->post("book_id"))) {
            $limit = REST_Controller::limitComment;
            $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
            $this_err = "";
            $ceklike = $this->db->query("SELECT * FROM ( SELECT * FROM comment $wheres  ORDER BY created_at DESC LIMIT ? ) sub ORDER BY created_at ASC", array($keyes, REST_Controller::limitComment));
            if ($ceklike->num_rows() > 0) {
                $resp_message = "show comment success";
                $resp_code = REST_Controller::HTTP_OK;
                foreach ($ceklike->result() as $dkey => $dval) {
                    $mads = array();
                    $mads["comment_id"] = $dval->comment_id;
                    $mads["comment_text"] = (string)$dval->comments;
                    $mads["comment_date"] = time_ago($dval->created_at);
                    $mads["comment_user_id"] = $dval->created;
                    $gus = $this->db->get_where("users", array("user_id" => (int)$dval->created));
                    if ($gus && $gus->num_rows() > 0) {
                        $mads["comment_user_name"] = $gus->row()->fullname;
                        $mads["comment_user_avatar"] = (string)$gus->row()->prof_pict;
                    } else {
                        $this_err = $this->db->error();
                        $this_err = ", but error on : " . implode(",", $this_err);
                    }
                    $this->db->where("id_comment", $dval->comment_id);
                    $getcm = $this->db->get("comment");
                    $mads["comment_reply_count"] = $getcm->num_rows();
                    if ($getcm->num_rows() > 0) {
                        foreach ($getcm->result() as $getkeys => $getval) {
                            $mads["comment_reply_data"][$getkeys]["comment_id"] = $getval->comment_id;
                            $mads["comment_reply_data"][$getkeys]["comment_text"] = $getval->comments;
                            $mads["comment_reply_data"][$getkeys]["comment_date"] = time_ago($getval->created_at);
                            $mads["comment_reply_data"][$getkeys]["comment_user_id"] = $getval->created;
                            $gusr = $this->db->get_where("users", array("user_id" => $getval->created));
                            if ($gusr->num_rows() > 0) {
                                $mads["comment_reply_data"][$getkeys]["comment_user_name"] = $gusr->row()->fullname;
                                $mads["comment_reply_data"][$getkeys]["comment_user_avatar"] = (string)$gusr->row()->prof_pict;
                            }
                        }
                    } else {
                        $mads["comment_reply_data"] = array();
                    }
                    $radata[] = $mads;
                }
            } else {
                $resp_message = "comment empty, start new comment";
                $resp_code = REST_Controller::HTTP_OK;
            }
            //checking if query executed
        } else {
            $resp_code = REST_Controller::HTTP_BAD_REQUEST;
            $resp_message = "book or paragraph cannot be empty";
        }
        $resp_data["comments"] = $radata;
        $resps = array("code" => $resp_code, "message" => $resp_message . $this_err, "data" => (object)$resp_data);
        set_resp($resps, $http_response_header);
    }

    public function shareBook_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $book_id = $this->post("book_id");
        $resp_message = "";
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        if (!empty($user_id) && !empty($book_id)) {
            $data_insert = array("id_user" => $user_id, "id_book" => $book_id);
            $ins = $this->db->insert("shares", $data_insert);
            //checking if query executed
            if ($ins) {
                $this->db->query("UPDATE book SET share_count = share_count + 1 WHERE book_id = ?", array($book_id));
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "share book success";
            } else {
                $array_error = $this->db->error();
                if (!empty($array_error)) {
                    $resp_code = $array_error["code"];
                    $resp_message = $array_error["message"];
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "book not found";
                }
            }
        } else {
            $resp_code = REST_Controller::HTTP_BAD_REQUEST;
            $resp_message = "user or book cannot be empty";
        }
        $resps = array("code" => $resp_code, "message" => $resp_message);
        set_resp($resps, $http_response_header);
    }

    public function bookmark_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $book_id = $this->post("book_id");
        $id_bookmark = $this->post("bookmark_id");
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        if (!empty($id_bookmark)) {
            $dels = $this->db->delete("bookmark", array("bookmark_id" => $id_bookmark));
            if ($dels) {
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "delete bookmark success";
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "id bookmark not found";
            }
        } else {
            if (!empty($user_id) && !empty($book_id)) {
                $data_insert = array("id_user" => $user_id, "id_book" => $book_id);
                $cekbookmark = $this->db->get_where("bookmark", $data_insert);
                if ($cekbookmark->num_rows() > 0) {
                    $dels = $this->db->delete("bookmark", $data_insert);
                    if ($dels) {
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resp_code = REST_Controller::HTTP_OK;
                        $resp_message = "delete bookmark success";
                    } else {
                        $resp_code = REST_Controller::HTTP_NOT_FOUND;
                        $resp_message = "id bookmark not found";
                    }
                } else {
                    $this->db->insert("bookmark", $data_insert);
                    //checking if query executed
                    $insid = $this->db->insert_id();
                    if ($insid) {
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resp_code = REST_Controller::HTTP_OK;
                        $resp_message = "bookmark success";
                    } else {
                        $array_error = $this->db->error();
                        if (!empty($array_error)) {
                            $resp_message = $array_error["code"] . "bookmark failed, because : " . $array_error["message"];
                        } else {
                            $resp_code = REST_Controller::HTTP_NOT_FOUND;
                            $resp_message = "book not found";
                        }
                    }
                }
            } else {
                $resp_code = REST_Controller::HTTP_BAD_REQUEST;
                $resp_message = "user or book cannot be empty";
            }
        }
        set_resp(array("code" => $resp_code, "message" => $resp_message, "data" => (object)array()), $http_response_header);
    }

    public function bestWriter_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $category_id = $this->post("category_id");
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_message = "set best writer success";
        $resp_data = array();
        $fiksi = array();
        $nonfiksi = array();
        $komik = array();
        $auth_id = array("1");
        if (!empty($user_id)) {
            if (!empty($this->post("category_id") && is_array($this->post("category_id")))) {
                $wherauth = "";
                if (in_array("1", $category_id, TRUE)) {
                    $count_fiksi = REST_Controller::limitFree;
                    if (in_array("2", $category_id) && !in_array("3", $category_id)) $count_fiksi = REST_Controller::limitFree - 1;
                    if (!in_array("2", $category_id) && in_array("3", $category_id)) $count_fiksi = REST_Controller::limitFree - 1;
                    if (in_array("2", $category_id) && in_array("3", $category_id)) $count_fiksi = REST_Controller::limitFree - 2;
                    if (!empty($auth_id)) {
                        $auths_id = implode(",", $auth_id);
                        $wherauth = "AND is_follow NOT IN ($auths_id)";
                    }
                    $where = "WHERE is_follow IN(SELECT author_id FROM book WHERE category_id = 1 AND author_id != ?) $wherauth";
                    $fixi = $this->db->query("SELECT is_follow as user_id, fullname, IFNULL(prof_pict, '') as prof_pict, COUNT(*) AS followers, (SELECT count(book_id) FROM book WHERE author_id = user_id AND status_publish !='draft') as book_made, 'fiksi' as category_name FROM follows INNER JOIN users ON follows.is_follow = users.user_id $where GROUP BY is_follow ORDER BY followers DESC LIMIT 0, $count_fiksi", array($user_id));
                    if ($fixi->num_rows() > 0) {
                        $fiksi = $fixi->result();
                        foreach ($fiksi as $fav) {
                            $auth_id[] = $fav->user_id;
                        }
                        $resp_data = array_merge($resp_data, $fixi->result_array());
                    }
                }
                if (in_array("2", $category_id, TRUE)) {
                    $count_nonfiksi = REST_Controller::limitFree;
                    $count_nonfiksi = $count_nonfiksi - count($fiksi);
                    if (!empty($auth_id)) {
                        $auths_id = implode(",", $auth_id);
                        $wherauth = "AND is_follow NOT IN ($auths_id)";
                    }
                    if (in_array("3", $category_id)) $count_nonfiksi = $count_nonfiksi - 1;
                    $where = "WHERE is_follow IN(SELECT author_id FROM book WHERE category_id != 1 AND category_id != 21 AND author_id != ?) $wherauth";
                    $nfixi = $this->db->query("SELECT is_follow as user_id, fullname, IFNULL(prof_pict, '') as prof_pict, COUNT(*) AS followers, (SELECT count(book_id) FROM book WHERE author_id = user_id AND status_publish !='draft') as book_made, 'non fiksi' as category_name FROM follows INNER JOIN users ON follows.is_follow = users.user_id $where GROUP BY is_follow ORDER BY followers DESC LIMIT 0, $count_nonfiksi", array($user_id));
                    if ($nfixi->num_rows() > 0) {
                        $nonfiksi = $nfixi->result();
                        $mathes = array();
                        foreach ($nonfiksi as $nfav) {
                            $mathes[] = $nfav->user_id;
                        }
                        array_merge($auth_id, $mathes);
                        $resp_data = array_merge($resp_data, $nfixi->result_array());
                    }
                }
                if (in_array("3", $category_id)) {
                    $cnfiksi = count($nonfiksi);
                    if (empty($cnfiksi)) $cnfiksi = count($fiksi);
                    $count_komik = REST_Controller::limitFree - $cnfiksi;
                    if (!empty($auth_id)) {
                        $auths_id = implode(",", $auth_id);
                        $wherauth = "AND is_follow NOT IN ($auths_id)";
                    }
                    $where = "WHERE is_follow IN(SELECT author_id FROM book WHERE category_id = 21  AND author_id != ?) $wherauth";
                    $kfixi = $this->db->query("SELECT is_follow as user_id, fullname, IFNULL(prof_pict, '') as prof_pict, COUNT(*) AS followers, (SELECT count(book_id) FROM book WHERE author_id = user_id AND status_publish !='draft') as book_made, 'komik' as category_name FROM follows INNER JOIN users ON follows.is_follow = users.user_id $where GROUP BY is_follow ORDER BY followers DESC LIMIT 0, $count_komik", array($user_id));
                    if ($kfixi->num_rows() > 0) {
                        $komik = $kfixi->result_array();
                        $resp_data = array_merge($resp_data, $komik);
                    }
                }
                //$resp_message .= " ".implode(",", $category_id);

                if (count($resp_data) > 0) {
                    // Set the response and exit
                    $resp_code = REST_Controller::HTTP_OK;
                } else {
                    // Set the response and exit
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "best writer not found";
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "category must not empty";
            }
        } else {
            $resp_message = 'No Request Detected';
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $http_response_header = REST_Controller::HTTP_ACCEPTED;
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, $http_response_header);
    }

    public function bestBook_get()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_message = "show best book success";
        $http_code_resp = REST_Controller::HTTP_ACCEPTED;
        $resp_data = (object)array();
        $count = 0;
        $limit = REST_Controller::limitFree;
        if (!empty($this->get("count")) && $this->get("count") > 1) {
            $mcount = (int)$this->get("count") - 1;
            $county = $mcount * $limit;
            $count = $county + 1;
        }
        $books = getPopularBook($count);
        if (!empty($books)) {
            // Set the response and exit
            $resp_data = $books;
        } else {
            // Set the response and exit
            $resp_message = 'No best books were found';
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, $http_response_header);
    }

    public function messageDelete_post()
    {
        $message_id = $this->post("message_id");
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_data = (object)array();
        $gbook = $this->db->get_where("messages", array("message_id" => $message_id));
        if (!empty($message_id)) {
            if ($gbook->num_rows() > 0) {
                $gchap = $this->db->get_where("messages", array("id_message" => $message_id));
                if ($gchap->num_rows() > 0) {
                    $this->db->delete("messages", array("id_message" => $message_id));
                }
                $gdbb = $this->db->delete("messages", array("message_id" => $message_id));
                if ($gdbb) {
                    $resp_message = "message has been deleted";
                    $resp_code = REST_Controller::HTTP_OK;
                    $http_response_header = REST_Controller::HTTP_OK;
                } else {
                    $errorrs = $this->db->error();
                    $errar = implode($errorrs, " : ");
                    $resp_message = "message cannot be deleted, because " . $errar;
                }
            } else {
                $resp_message = "message not found";
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
            }
        } else {
            $user_to = $this->post("user_to");
            $this->db->where("(user_from = " . $user_id . " OR user_to = " . $user_id . ") AND (user_from = " . $user_to . " OR user_to = " . $user_to . ")");
            $gchap = $this->db->get("messages");
            if ($gchap && $gchap->num_rows() > 0) {
                $gdbb = $this->db->delete("messages", "(user_from = " . $user_id . " OR user_to = " . $user_id . ") AND (user_from = " . $user_to . " OR user_to = " . $user_to . ")");
                if ($gdbb) {
                    $resp_message = "message has been deleted";
                    $resp_code = REST_Controller::HTTP_OK;
                    $http_response_header = REST_Controller::HTTP_OK;
                } else {
                    $errorrs = $this->db->error();
                    $errar = implode($errorrs, " : ");
                    $resp_message = "message cannot be deleted, because " . $errar;
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "user id or friend not found";
            }
        }
        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($resps, $http_response_header);
    }

    public function index_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_OK;
        $limit = REST_Controller::limit_timeline;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        if (!empty($this->post("fcm_id")) && !empty($this->post("device_id")) && !empty($user_id)) {
            $fcms = setDeviceFCM($user_id, $this->post("fcm_id"), $this->post("device_id"));
        }
        $url_event = array(
            "image" => "",
            "redirect" => ""
        );
        $top_event = topEvent();
        if (count($top_event) > 0) {
            $url_event = array(
                "image" => $top_event["img_mobile"],
                "redirect" => $top_event["url_redirect"]
            );
        }
        $selysih = 0;
        $selisih = 0;
        $bukuteman = $this->db->query("SELECT COUNT(book_id) as totalbukuteman FROM book WHERE book.status_publish != 'draft' AND (book.author_id IN (SELECT is_follow FROM follows WHERE created_by = '" . $user_id . "') OR book.author_id = '" . $user_id . "')");
        $total_bukuteman = ($bukuteman && $bukuteman->num_rows() > 0) ? $bukuteman->row()->totalbukuteman : 0;
        $resp_data = array();
        $this->db->limit(REST_Controller::limit_timeline, $count);
        $this->db->order_by("RAND()");
        $this->db->where("book.status_publish != 'draft' AND (book.author_id IN (SELECT is_follow FROM follows WHERE created_by = '" . $user_id . "') OR book.author_id = '" . $user_id . "')");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->select("book_id, title_book, author_id, fullname, prof_pict, category_id, book_type, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions");
        $timeline = $this->db->get("book");
        $setMax = REST_Controller::limit_timeline;
        $selasih = REST_Controller::limitNotFriend;
        $pageStop = round($selasih / $setMax);
        if ($selasih > $total_bukuteman) {
            $selysih = $selasih - $total_bukuteman;
            if ($setMax > $timeline->num_rows()) {
                $selisih = $setMax - $timeline->num_rows();
            }
        }
        $book_ids = array();
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($timeline && $timeline->num_rows() > 0) {
            // Set the response and exit
            foreach ($timeline->result() as $bokeys => $boval) {
                $is_bookmark = checkIsBookmark($boval->book_id, $user_id);
                $is_likes = checkIsLike($boval->book_id, $user_id);
                $getComment = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($boval->book_id));
                $res_par_text = getParagraphByBook($boval->book_id);
                $cat_id = 1;
                $publish_date = time_ago($boval->publish_at);
                $latest_update = $publish_date;
                if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                if (!empty($boval->category_id)) $cat_id = $boval->category_id;
                $category = getCategoryBook($cat_id);
                $book_ids[] = $boval->book_id;
                $dasray = array(
                    "book_id" => $boval->book_id,
                    "author_id" => $boval->author_id,
                    "author_name" => $boval->fullname,
                    "author_avatar" => (string)$boval->prof_pict,
                    "cover_url" => (string)$boval->file_cover,
                    "image_url" => (string)$boval->image_url,
                    "title_book" => (string)$boval->title_book,
                    "category" => $category,
                    "is_like" => $is_likes,
                    "is_bookmark" => $is_bookmark,
                    "book_type" => (int)$boval->book_type,
                    "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                    "latest_update" => $latest_update,
                    "publish_date" => time_ago($boval->publish_at),
                    "ptime" => strtotime($boval->publish_at),
                    "like_count" => (int)$boval->like_count,
                    "comment_count" => ($getComment && $getComment->num_rows() > 0) ? (int)$getComment->row()->comment_count : 0,
                    "view_count" => (int)$boval->view_count,
                    "share_count" => (int)$boval->share_count,
                    "desc" => $res_par_text
                );
                $resp_data[] = $dasray;
            }
        }
        //cek lanjutan
        if ($selisih > 0 && $this->post("count") <= $pageStop) {
            $not_in = "";
            $book_id = implode(",", $book_ids);
            if (!empty($book_id)) $not_in = " AND book_id NOT IN ($book_id)";
            $booksy = $this->db->query("SELECT book_id, title_book, author_id, fullname, prof_pict, category_id, book_type, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions FROM `book` INNER JOIN users ON book.author_id = users.user_id WHERE status_publish != 'draft' $not_in ORDER BY RAND() DESC LIMIT ?, ?", array($count, $selisih));
            if ($booksy && $booksy->num_rows() > 0) {
                foreach ($booksy->result() as $bokexs => $bovalx) {
                    $is_bookmarkx = checkIsBookmark($bovalx->book_id, $user_id);
                    $is_likesx = checkIsLike($bovalx->book_id, $user_id);
                    $getCommentx = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($bovalx->book_id));
                    $res_par_texty = getParagraphByBook($bovalx->book_id);
                    $cat_idx = 1;
                    $publish_datex = time_ago($bovalx->publish_at);
                    $latest_updatex = $publish_datex;
                    if (!empty($bovalx->latest_update) && $bovalx->latest_update != $bovalx->publish_at && $bovalx->status_publish == "publish") $latest_updatex = time_ago($bovalx->latest_update);
                    if (!empty($bovalx->category_id)) $cat_idx = $bovalx->category_id;
                    $categoryx = getCategoryBook($cat_idx);
                    $dasrayes = array(
                        "book_id" => $bovalx->book_id,
                        "author_id" => $bovalx->author_id,
                        "author_name" => $bovalx->fullname,
                        "author_avatar" => (string)$bovalx->prof_pict,
                        "cover_url" => (string)$bovalx->file_cover,
                        "image_url" => (string)$bovalx->image_url,
                        "title_book" => (string)$bovalx->title_book,
                        "category" => $categoryx,
                        "is_like" => $is_likesx,
                        "is_bookmark" => $is_bookmarkx,
                        "book_type" => (int)$bovalx->book_type,
                        "is_pdf" => (!empty($bovalx->is_pdf)) ? false : true,
                        "latest_update" => $latest_updatex,
                        "publish_date" => time_ago($bovalx->publish_at),
                        "ptime" => strtotime($bovalx->publish_at),
                        "like_count" => (int)$bovalx->like_count,
                        "comment_count" => ($getCommentx && $getCommentx->num_rows() > 0) ? (int)$getCommentx->row()->comment_count : 0,
                        "view_count" => (int)$bovalx->view_count,
                        "share_count" => (int)$bovalx->share_count,
                        "desc" => $res_par_texty
                    );
                    $resp_data[] = $dasrayes;
                }
            }
        }
        //check count
        if (count($resp_data) > 0) {
            usort($resp_data, function ($a, $b) {
                return (int)$b['ptime'] - (int)$a['ptime'];
            });
            foreach (array_keys($resp_data) as $mpakey) {
                unset($resp_data[$mpakey]["ptime"]);
                $populares = (object)array();
                $papakey = $mpakey + 1;
                if ($papakey == REST_Controller::popularBook_position) {
                    $desc = "Popular Books";
                    $bookzy = getPopularBook();
                    $populares = (object)array("desc" => $desc, "data" => $bookzy);
                }
                $resp_data[$mpakey]["populars"] = $populares;
            }
            $resp_message = "show timeline success";
            $http_code_resp = REST_Controller::HTTP_OK;
        } else {
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "no timeline data";
        }

        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => array(
                "event" => $url_event,
                "timeline" => $resp_data
            )
        );
        set_resp($responses, $http_response_header);
    }

    public function home_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_OK;
        $limit = REST_Controller::limit_timeline;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        if (!empty($this->post("fcm_id")) && !empty($this->post("device_id")) && !empty($user_id)) {
            $fcms = setDeviceFCM($user_id, $this->post("fcm_id"), $this->post("device_id"));
        }
        $url_event = array(
            "image" => "",
            "redirect" => ""
        );
        $top_event = topEvent();
        if (count($top_event) > 0) {
            $url_event = array(
                "image" => $top_event["img_mobile"],
                "redirect" => $top_event["url_redirect"]
            );
        }
        $selysih = 0;
        $selisih = 0;
        $bukuteman = $this->db->query("SELECT COUNT(book_id) as totalbukuteman FROM book WHERE book.status_publish != 'draft' AND (book.author_id IN (SELECT is_follow FROM follows WHERE created_by = '" . $user_id . "') OR book.author_id = '" . $user_id . "')");
        $total_bukuteman = ($bukuteman && $bukuteman->num_rows() > 0) ? $bukuteman->row()->totalbukuteman : 0;
        $resp_data = array();
        $this->db->limit(REST_Controller::limit_timeline, $count);
        $this->db->order_by("RAND()");
        $this->db->where("book.status_publish != 'draft' AND (book.author_id IN (SELECT is_follow FROM follows WHERE created_by = '" . $user_id . "') OR book.author_id = '" . $user_id . "')");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->select("book_id, title_book, author_id, fullname, prof_pict, category_id, book_type, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions");
        $timeline = $this->db->get("book");
        $setMax = REST_Controller::limit_timeline;
        $selasih = REST_Controller::limitNotFriend;
        $pageStop = round($selasih / $setMax);
        if ($selasih > $total_bukuteman) {
            $selysih = $selasih - $total_bukuteman;
            if ($setMax > $timeline->num_rows()) {
                $selisih = $setMax - $timeline->num_rows();
            }
        }
        $book_ids = array();
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($timeline && $timeline->num_rows() > 0) {
            // Set the response and exit
            foreach ($timeline->result() as $bokeys => $boval) {
                $is_bookmark = checkIsBookmark($boval->book_id, $user_id);
                $is_likes = checkIsLike($boval->book_id, $user_id);
//                $getComment = $this->db->get_where("comment", array("id_book" => $boval->book_id));
                $getComment = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($boval->book_id));
                if($boval->is_pdf == 1) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = substr((string)$boval->descriptions, 0, REST_Controller::text_limits) . "...";
                $category = getCategoryBook($boval->category_id);
                $is_download = false;
                $this->db->select("is_download");
                $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $boval->book_id));
                if ($cekBought->num_rows() > 0) {
                    if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                }
                $publish_date = time_ago($boval->publish_at);
                $latest_update = $publish_date;
                if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                $book_ids[] = $boval->book_id;
                $dasray = array(
                    "book_id" => $boval->book_id,
                    "author_id" => $boval->author_id,
                    "author_name" => $boval->fullname,
                    "author_avatar" => (string)$boval->prof_pict,
                    "cover_url" => (string)$boval->file_cover,
                    "image_url" => (string)$boval->image_url,
                    "title_book" => (string)$boval->title_book,
                    "category" => $category,
                    "is_like" => $is_likes,
                    "is_bookmark" => $is_bookmark,
                    "is_download" => $is_download,
                    "book_type" => (int)$boval->book_type,
                    "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                    "latest_update" => $latest_update,
                    "publish_date" => time_ago($boval->publish_at),
                    "ptime" => strtotime($boval->publish_at),
                    "like_count" => (int)$boval->like_count,
                    "comment_count" => ($getComment && $getComment->num_rows() > 0) ? (int)$getComment->row()->comment_count : 0,
                    "view_count" => (int)$boval->view_count,
                    "share_count" => (int)$boval->share_count,
                    "desc" => $res_par_text
                );
                $resp_data[] = $dasray;
            }
        }
        //cek lanjutan
        if ($selisih > 0 && $this->post("count") <= $pageStop) {
            $not_in = "";
            $book_id = implode(",", $book_ids);
            if (!empty($book_id)) $not_in = " AND book_id NOT IN ($book_id)";
            $booksy = $this->db->query("SELECT book_id, title_book, author_id, fullname, prof_pict, category_id, book_type, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions FROM `book` INNER JOIN users ON book.author_id = users.user_id WHERE status_publish != 'draft' $not_in ORDER BY RAND() DESC LIMIT ?, ?", array($count, $selisih));
            if ($booksy && $booksy->num_rows() > 0) {
                foreach ($booksy->result() as $bokexs => $bovalx) {
                    $is_bookmarkx = checkIsBookmark($bovalx->book_id, $user_id);
                    $is_likesx = checkIsLike($bovalx->book_id, $user_id);
//                    $getCommentx = $this->db->get_where("comment", array("id_book" => $bovalx->book_id));
                    $getCommentx = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($bovalx->book_id));
                    if($bovalx->is_pdf == 1) $res_par_texty = getParagraphByBook($bovalx->book_id);
                    else $res_par_texty = substr((string)$bovalx->descriptions, 0, REST_Controller::text_limits) . "...";
                    $categoryx = getCategoryBook($bovalx->category_id);
                    $is_downloadx = false;
                    $this->db->select("is_download");
                    $cekBoughtx = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $bovalx->book_id));
                    if ($cekBoughtx->num_rows() > 0) {
                        if (isset($cekBoughtx->row()->is_download) && $cekBoughtx->row()->is_download == 1) $is_downloadx = true;
                    }
                    $publish_datex = time_ago($bovalx->publish_at);
                    $latest_updatex = $publish_datex;
                    if (!empty($bovalx->latest_update) && $bovalx->latest_update != $bovalx->publish_at && $bovalx->status_publish == "publish") $latest_updatex = time_ago($bovalx->latest_update);
                    $dasrayes = array(
                        "book_id" => $bovalx->book_id,
                        "author_id" => $bovalx->author_id,
                        "author_name" => $bovalx->fullname,
                        "author_avatar" => (string)$bovalx->prof_pict,
                        "cover_url" => (string)$bovalx->file_cover,
                        "image_url" => (string)$bovalx->image_url,
                        "title_book" => (string)$bovalx->title_book,
                        "category" => $categoryx,
                        "is_like" => $is_likesx,
                        "is_bookmark" => $is_bookmarkx,
                        "is_download" => $is_downloadx,
                        "book_type" => (int)$bovalx->book_type,
                        "is_pdf" => (!empty($bovalx->is_pdf)) ? false : true,
                        "latest_update" => $latest_updatex,
                        "publish_date" => time_ago($bovalx->publish_at),
                        "ptime" => strtotime($bovalx->publish_at),
                        "like_count" => (int)$bovalx->like_count,
//                        "comment_count" => $getCommentx->num_rows(),
                        "comment_count" => ($getCommentx && $getCommentx->num_rows() > 0) ? (int)$getCommentx->row()->comment_count : 0,
                        "view_count" => (int)$bovalx->view_count,
                        "share_count" => (int)$bovalx->share_count,
                        "desc" => $res_par_texty
                    );
                    $resp_data[] = $dasrayes;
                }
            }
        }
        //check count
        if (count($resp_data) > 0) {
            usort($resp_data, function ($a, $b) {
                return (int)$b['ptime'] - (int)$a['ptime'];
            });
            foreach (array_keys($resp_data) as $mpakey) {
                unset($resp_data[$mpakey]["ptime"]);
                $populares = (object)array();
                $papakey = $mpakey + 1;
                if ($papakey == REST_Controller::popularBook_position) {
                    if ($this->post("count") > 0 && $this->post("count") % 2 == 0) {
                        $desc = "Popular Writers";
                        $bookzy = getMobilePopularWriter();
                        $populares = (object)array("desc" => $desc, "data" => $bookzy);
                    } else {
                        $desc = "Popular Books";
                        $bookzy = getPopularBook();
                        $populares = (object)array("desc" => $desc, "data" => $bookzy);
                    }
                }
                $resp_data[$mpakey]["populars"] = $populares;
            }
            $resp_message = "show timeline success";
            $http_code_resp = REST_Controller::HTTP_OK;
        } else {
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "no timeline data";
        }
//        $resp_message .= " total buku teman : ".$total_bukuteman; #debug jumlah buku teman
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => array(
                "event" => $url_event,
                "timeline" => $resp_data
            )
        );
        set_resp($responses, $http_response_header);
    }

    public function searchUsers_post()
    {
        $search_results = array();
        $body = (object)$this->post();
        $count = 0;
        $limit = REST_Controller::limitsearchUser;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;

        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        //set search limit
        $this->db->limit($limit, $count);
        $this->db->where("fullname LIKE '%" . $body->search . "%'");
        $this->db->select("user_id, role_id, fullname, email, IFNULL(date_of_birth, '') as date_of_birth,  IFNULL(jk, '') as jk, IFNULL(prof_pict, '') as prof_pict, IFNULL(address, '') as address,  IFNULL(about_me, '') as about_me, book_sold, balance, IFNULL(phone, '') as phone, oauth_uid, oauth_provider");
        $susr = $this->db->get("users");
        $sarus = array();
        if ($susr->num_rows() > 0) {
            foreach ($susr->result() as $sasrow) {
                $arus = array();
                $is_follow = false;
                $cekfollow = $this->db->get_where("follows", array("created_by" => $user_id, "is_follow" => $sasrow->user_id));
                if ($cekfollow->num_rows() > 0) $is_follow = true;
                $followers = $this->db->get_where("follows", array("is_follow" => $sasrow->user_id))->num_rows();
                $book_made = $this->db->get_where("book", array("author_id" => $sasrow->user_id, "status_publish != " => "draft"))->num_rows();
                $arus["user_id"] = $sasrow->user_id;
                $arus["role_id"] = $sasrow->role_id;
                $arus["fullname"] = (string)$sasrow->fullname;
                $arus["email"] = $sasrow->email;
                $arus["prof_pict"] = (string)$sasrow->prof_pict;
                $arus["date_of_birth"] = (string)$sasrow->date_of_birth;
                $arus["jk"] = (string)$sasrow->jk;
                $arus["address"] = (string)$sasrow->address;
                $arus["about_me"] = (string)$sasrow->about_me;
                $arus["oauth_uid"] = (string)$sasrow->oauth_uid;
                $arus["oauth_provider"] = (string)$sasrow->oauth_provider;
                $arus["followers"] = $followers;
                $arus["book_sold"] = (int)$sasrow->book_sold;
                $arus["book_made"] = $book_made;
                $arus["balance"] = (int)$sasrow->balance;
                $arus["is_newuser"] = false;
                $arus["isFollow"] = $is_follow;
                $arus["has_pin"] = false;
                $arus["is_activated"] = false;
                $sarus[] = $arus;
            }
        }
        $search_results = $sarus;
        if (count($search_results) > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps["code"] = REST_Controller::HTTP_OK;
            $resps["message"] = "";
            $resps["data"] = (array)$search_results;
        } else {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps["code"] = REST_Controller::HTTP_OK;
            $resps["message"] = "search result not found";
            $resps["data"] = (array)$search_results;
        }
        set_resp($resps, $http_response_header);
    }

    public function searchBooks_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $search_results = array();
        $body = (object)$this->post();
        $limit = REST_Controller::limitsearchBook;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;

        $sabook = array();
        $where = "title_book LIKE '%" . $body->search . "%'";
        $books = (array)$this->setBook($where, 0, $user_id);
        if (is_array($books) && count($books) > 0) {
            $sabook = json_decode(json_encode($books), true);
        }
        //check if $sabook is not empty
        if (!empty($sabook)) $search_results = array_map("unserialize", array_unique(array_map("serialize", $sabook)));
//        $size = $count - count($search_results);
        if (count($search_results) > 0 && $count <= count($search_results)) $search_results = array_slice($search_results, $count, $limit);
        if (count($search_results) > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps["code"] = REST_Controller::HTTP_OK;
            $resps["message"] = "";
            $resps["data"] = (array)$search_results;
        } else {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps["code"] = REST_Controller::HTTP_OK;
            $resps["message"] = "search result not found";
            $resps["data"] = (array)$search_results;
        }
        set_resp($resps, $http_response_header);
    }
}