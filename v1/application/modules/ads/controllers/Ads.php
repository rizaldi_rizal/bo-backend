<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Ads extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper("baboo");
        $this->load->library("S3", "s3");
        $this->methods['index_get']['limit'] = 500; // 500 requests per hour per user/key
    }

    public function index_get()
    {
        $dasray = array();
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_NOT_FOUND;
        $resp_message = "data ads not found";
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $this->db->limit(REST_Controller::limit_ads, 0);
        $this->db->order_by("ads_end", "DESC");
        $this->db->select("ads_id, ads_name, ads_book, ads_start, ads_end, ads_image, ads_url");
        $get_ads = $this->db->get_where("baboo_ads", "NOW() >= ads_start AND NOW() <= ads_end");
        if ($get_ads && $get_ads->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = $http_response_header;
            $resp_message = "list ads success";
            foreach ($get_ads->result() as $adsval) {
                $book_id = $adsval->ads_book;
                $ads_name = $adsval->ads_name;
                $author_id = "";
                $ads_creator = "";
                $ads_avatar = "";
                $cover_url = "";
                $img_url = "";
                $cat_id = 1;
                $is_likes = false;
                $is_pdf = false;
                $is_bookmark = false;
                $publish_date = time_ago(date("Y-m-d H:i:s"));
                $latest_update = $publish_date;
                $count_comment = 0;
                $like_count = 0;
                $share_count = 0;
                $view_count = 0;
                $res_par_text = "";
                if(!empty($book_id)){
                    $this->db->join("users", "users.user_id = book.author_id");
                    $this->db->select("book_id, title_book, author_id, fullname, prof_pict, category_id, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions");
                    $gbook = $this->db->get_where("book", array("book_id"=>$book_id));
                    if($gbook && $gbook->num_rows() > 0)
                    {
                        $ads_name = $gbook->row()->title_book;
                        $author_id = $gbook->row()->author_id;
                        $ads_creator = $gbook->row()->fullname;
                        $ads_avatar = $gbook->row()->prof_pict;
                        $cover_url = $gbook->row()->file_cover;
                        $like_count = $gbook->row()->like_count;
                        $share_count = $gbook->row()->share_count;
                        $view_count = $gbook->row()->view_count;
                        $getComment = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($book_id));
                        $count_comment = ($getComment && $getComment->num_rows() > 0) ? (int)$getComment->row()->comment_count : 0;
                        $img_url = $gbook->row()->image_url;
                        if (!empty($gbook->row()->category_id)) $cat_id = $gbook->row()->category_id;
                        $publish_date = time_ago($gbook->row()->publish_at);
                        $latest_update = $publish_date;
                        if (!empty($gbook->row()->latest_update) && $gbook->row()->latest_update != $gbook->row()->publish_at && $gbook->row()->status_publish == "publish") $latest_update = time_ago($gbook->row()->latest_update);
                        $is_likes = checkIsLike($gbook->row()->book_id, $user_id);
                        $is_bookmark = checkIsBookmark($gbook->row()->book_id, $user_id);
                        $is_pdf = (!empty($gbook->row()->is_pdf)) ? false : true;
                        //checking background
                        if(!empty($gbook->row()->is_pdf)) $res_par_text = getParagraphByBook($book_id);
                        else $res_par_text = (string)$gbook->row()->descriptions;
                    }
                }
                $category = getCategoryBook($cat_id);
                $dasray[] = array(
                    "book_id" => $book_id,
                    "author_id" => $author_id,
                    "author_name" => $ads_creator,
                    "author_avatar" => (string)$ads_avatar,
                    "cover_url" => (string)$cover_url,
                    "image_url" => (string)$img_url,
                    "title_book" => (string)$ads_name,
                    "category" => $category,
                    "is_like" => $is_likes,
                    "is_bookmark" => $is_bookmark,
                    "is_pdf" => $is_pdf,
                    "latest_update" => $latest_update,
                    "publish_date" => $publish_date,
                    "like_count" => (int)$like_count,
                    "comment_count" => (int)$count_comment,
                    "view_count" => (int)$view_count,
                    "share_count" => (int)$share_count,
                    "desc" => $res_par_text
                );
            }
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $dasray
        );
        set_resp($responses, $http_response_header);
    }
}