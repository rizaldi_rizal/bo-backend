<?php
/**
 * Created by Abyan.
 * User: Win_10
 * Date: 4/13/2018
 * Time: 4:09 PM
 */
require APPPATH . 'libraries/REST_Controller.php';

class Iris extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper("baboo");
        $config = iris_config();
        $this->server_key = isset($config["server_key"]) ? $config["server_key"] : "";
        $this->api_url = isset($config["api_url"]) ? $config["api_url"] : "";
    }

    public function listBank_get()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_data = array();
        $banks = $this->db->get("banks");
        if ($banks->num_rows() > 0) {
            foreach ($banks->result() as $cekval) {
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "show list banks success";
                $resp_data[] = array(
                    "bank_code" => $cekval->kode_bank,
                    "bank_name" => $cekval->bank_name,
                );
            }
        } else {
            $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
            $resp_message = "no bank data existed";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        if (!empty($user_id)) set_resp($responses, REST_Controller::HTTP_OK);
        else $this->set_response($responses, REST_Controller::HTTP_OK);
    }

    public function createBeneficiaries_post()
    {
        $post = $this->post();
        $key = sethead();
        $user_id = getUserFromKey($key);
        $arr = array();
        if (!empty($user_id)) {
            $user_data = getUsersById($user_id);
            $fullname = $post["account_name"];
            $email = "";
            $alias = "";
            if (!empty($user_data)) {
                if (empty($fullname)) $fullname = $user_data["fullname"];
                $email = $user_data["email"];
            }

            if (!empty($fullname) && isset($post["bank_code"]) && !empty($post["bank_code"])) {
                $alias = str_replace(' ', '', $fullname) . $post["bank_code"];
                $alias = strtolower($alias);
                if (strlen($alias) > 20) {
                    $alias = substr($alias, -5);
                }
            }
            $request_for = array(
                "bank" => $post["bank_code"],
                "account" => $post["account_number"]
            );
            $checkacc = $this->db->get_where("user_acc", array("bank_code"=>$post["bank_code"], "account_no"=>$post["account_number"]));
            if ($checkacc->num_rows() == 0) {
                $api_check = $this->api_url . "/api/v1/account_validation?bank=" . $post["bank_code"] . "&account=" . $post["account_number"];
                $recheck = $this->iris_api($api_check, "GET", $request_for);
                $rsr = json_decode($recheck);
                if ($rsr->account_no) {
                    $request_body = array(
                        "name" => $fullname,
                        "account" => $post["account_number"],
                        "bank" => $post["bank_code"],
                        "alias_name" => $alias,
                        "email" => $email
                    );
                    $api_url = $this->api_url . "/api/v1/beneficiaries";
                    $result = $this->iris_api($api_url, "POST", $request_body);
                    if ($result) {
                        $jsresult = json_decode($result);
                        if ($jsresult->status == "created") {
                            $data_acc["account_no"] = $post["account_number"];
                            $data_acc["bank_code"] = $post["bank_code"];
                            $data_acc["account_name"] = $fullname;
                            $data_acc["created_by"] = $user_id;
                            $cek_acc = $this->db->get_where("user_acc", $data_acc);
                            if ($cek_acc && $cek_acc->num_rows() == 0) {
                                $this->db->insert("user_acc", $data_acc);
                                $accid = $this->db->insert_id();
                                if ($accid) {
                                    $user_acc_history = array(
                                        "acc_id" => $accid,
                                        "id_user" => $user_id
                                    );
                                    $this->db->insert("user_acc_history", $user_acc_history);
                                    $data_start["id_acc"] = $accid;
                                    $data_start["account_number"] = $post["account_number"];
                                    $data_start["account_name"] = $fullname;
                                    $banks = $this->db->get_where("banks", array("kode_bank" => $post["bank_code"]));
                                    $bank_name = ($banks && $banks->num_rows() > 0) ? $banks->row()->bank_name : $post["bank_code"];
                                    $data_start["bank_name"] = $bank_name;
                                    $arr = $data_start;
                                }
                            }
                            $resp_code = REST_Controller::HTTP_OK;
                            $resp_message = "beneficiary created";
                        } else {
                            $resp_code = REST_Controller::HTTP_MULTIPLE_CHOICES;
                            $resp_message = $result;
                        }
                    } else {
                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                        $resp_message = $result;
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "akun tidak terdaftar di bank yang tersedia";
                    $arr = $rsr;
                }
            } else {
                $crow = $checkacc->row();
                if ($crow->account_name == $fullname) {
                    $user_acc_history = array(
                        "acc_id" => $crow->id_acc,
                        "id_user" => $user_id
                    );
                    $check_users = $this->db->get_where("user_acc_history", $user_acc_history);
                    if($check_users->num_rows() == 0){
                        $this->db->insert("user_acc_history", $user_acc_history);
                        $acc_usr = $this->db->insert_id();
                        if ($acc_usr) {
                            $resp_code = REST_Controller::HTTP_OK;
                            $resp_message = "beneficiary created";
                            $data_start["id_acc"] = $crow->id_acc;
                            $data_start["account_number"] = $post["account_number"];
                            $data_start["account_name"] = $fullname;
                            $banks = $this->db->get_where("banks", array("kode_bank" => $post["bank_code"]));
                            $bank_name = ($banks && $banks->num_rows() > 0) ? $banks->row()->bank_name : $post["bank_code"];
                            $data_start["bank_name"] = $bank_name;
                            $arr = $data_start;
                        } else {
                            $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                            $resp_message = "akun terdaftar tetapi ada yang salah ketika memasukkan ke database kami " . implode(",", $this->db->error());
                        }
                    }else{
                        $resp_code = REST_Controller::HTTP_MULTIPLE_CHOICES;
                        $resp_message = "akun ini sudah terdaftar di list anda";
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_MULTIPLE_CHOICES;
                    $resp_message = "akun terdaftar tetapi dengan nama yang berbeda";
                }
            }
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "user not authorized or empty";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$arr
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    private function BabooPay_post($data_post, $userID = '')
    {
        if (empty($userID)) {
            $keys = sethead();
            $userID = getUserFromKey($keys);
        }

        $stat = FALSE;
        if (!empty($data_post)) {
            $data_insert = array();
            if (isset($data_post["transaction_id"])) $data_insert["transaction_id"] = $data_post["transaction_id"];
            $data_insert["payment_type"] = "payout";
            $data_insert["transaction_status"] = "queued";
            if (isset($data_post["amount"])) $data_insert["gross_amount"] = $data_post["amount"];
            if (!empty($userID)) $data_insert["user_id"] = $userID;
            $data_insert["transaction_time"] = date("Y-m-d H:i:s");
            $data_insert["status_code"] = REST_Controller::HTTP_CREATED;
            if (isset($data_post["bank"])) $data_insert["bank"] = $data_post["bank"];
            if (isset($data_post["account_no"])) $data_insert["va_numbers"] = $data_post["account_no"];
            $ins = $this->db->insert("midtrans", $data_insert);
            if ($ins) {
                $stat = TRUE;
            }
        }
        return $stat;
    }

    public function createPayout_post()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $post = $this->post();
        $key = sethead();
        $user_id = getUserFromKey($key);
        $resp_data = array();
        if (!empty($user_id)) {
            $fullname = "";
            $account_no = (!empty($post["account_number"])) ? $post["account_number"] : "";
            $bank = (!empty($post["bank_code"])) ? $post["bank_code"] : "";
            $email = "";
            $balance = 0;
            if (!empty($this->post("id_acc")) || !empty($this->post("account_number"))) {
                if (!empty($this->post("account_number"))) $this->db->where("account_no", $this->post("account_number"));
                if (!empty($this->post("id_acc"))) $this->db->where("id_acc", $this->post("id_acc"));
                $cekdis = $this->db->get("user_acc");
                if ($cekdis && $cekdis->num_rows() > 0) {
                    $fullname = $cekdis->row()->account_name;
                    $account_no = $cekdis->row()->account_no;
                    $bank = $cekdis->row()->bank_code;
                    $user_created = $cekdis->row()->created_by;
                    if(!empty($user_created)){
                        $usercreacc = getUsersById($user_created);
                        $email = (!empty($usercreacc) && count($usercreacc) > 0) ? $usercreacc["email"] : "";
                    }
                }
            }

            $user_data = getUsersById($user_id);
            if (!empty($user_data)) {
                if (empty($fullname)) $fullname = $user_data["fullname"];
                $email = (empty($email)) ? $user_data["email"] : $email;
                $balance = $user_data["balance"];
            }

            $this->db->where("transaction_status", "queued");
            $this->db->where("payment_type", "payout");
            $gpending = $this->db->get_where("midtrans", array("user_id" => $user_id));
            if ($gpending->num_rows() == 0) {
                if (!empty($balance) && $balance >= REST_Controller::minimum_wadges) {
                    $payouts[] = array(
                        "beneficiary_name" => $fullname,
                        "beneficiary_account" => $account_no,
                        "beneficiary_bank" => $bank,
                        "beneficiary_email" => $email,
                        "amount" => $post["amount"],
                        "notes" => "payout " . date("F d")
                    );
                    $request_body = array(
                        "payouts" => (array)$payouts
                    );
                    $api_url = $this->api_url . "/api/v1/payouts";
                    $result = $this->iris_api($api_url, "POST", $request_body);
                    if ($result) {
                        $jsresult = json_decode($result);
                        if ($jsresult->payouts[0]->status == "queued") {
                            //insert to midtrans tbl
                            $data_insert = array(
                                "transaction_id" => $jsresult->payouts[0]->reference_no,
                                "account_no" => $account_no,
                                "bank" => $bank,
                                "amount" => $post["amount"],
                            );
                            $stats = $this->BabooPay_post($data_insert, $user_id);
                            if ($stats) {
                                $resp_code = REST_Controller::HTTP_OK;
                                $resp_message = "payouts created";
                            } else {
                                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                                $resp_message = "error on db " . implode(",", $this->db->error());
                            }

                            $http_response_header = REST_Controller::HTTP_OK;
                            $resp_data = json_decode($result);
                        } else {
                            $resp_code = REST_Controller::HTTP_MULTIPLE_CHOICES;
                            $resp_message = $result;
                        }
                    } else {
                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                        $resp_message = $result;
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    $resp_message = "balance not exceed minimum withdrawal, minimum withdrawal is Rp. " . number_format(REST_Controller::minimum_wadges) . ", your balance is " . $balance;
                }
            } else {
                $resp_code = REST_Controller::HTTP_BAD_REQUEST;
                $resp_message = "you still had one pending payout, please wait or contact admin";
            }
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "user not authorized or empty";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($responses, $http_response_header);
    }

    public function updatePayout_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $data_post = $this->post();
        if (!empty($data_post)) {
            $data_insert = array();
            $data_where = array();
            if (isset($data_post["reference_no"])) {
                $data_where["transaction_id"] = $data_post["reference_no"];
                $data_insert["transaction_id"] = $data_post["reference_no"];
            }
            if (isset($data_post["status"]) && !empty($data_post["status"])) $data_insert["transaction_status"] = $data_post["status"];
            if (isset($data_post["amount"])) $data_insert["gross_amount"] = $data_post["amount"];
            $data_insert["status_code"] = REST_Controller::HTTP_OK;
            $ins = $this->db->update("midtrans", $data_insert, $data_where);
            if ($ins) {
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "update success";
                $gmid = $this->db->get_where("midtrans", $data_where);
                if ($gmid && $gmid->num_rows() > 0) {
                    $status = "sedang diproses...";
                    if ($gmid->row()->transaction_status == "completed") {
                        $status = "berhasil, silahkan cek bank anda";
                        $this->db->query("UPDATE users SET balance = balance - " . (double)$gmid->row()->gross_amount . " WHERE user_id = " . $gmid->row()->user_id);
                    } elseif ($gmid->row()->transaction_status == "failed") {
                        $status = "gagal, silahkan kontak administrator";
                    }
                    $notif_text = "penarikan dana akun anda sebesar " . $gmid->row()->gross_amount . " " . $status;
                    $arr_notif = array(
                        "notif_type" => REST_Controller::type_payout,
                        "notif_to" => $gmid->row()->user_id,
                        "notif_by" => 1,
                        "notif_text" => $notif_text
                    );
                    $this->db->insert("notifications", $arr_notif);
                    $insd = $this->db->insert_id();
                    if ($insd) {
                        setpush_notif($gmid->row()->user_id, $notif_text, REST_Controller::type_payout, $gmid->row()->user_id);
                    }
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "update failed";
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "no data found";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
        );
        $this->set_response($responses, $http_response_header);
    }

    public function historyPayout_get()
    {
        $key = sethead();
        $user_id = getUserFromKey($key);
        $resp_data = array();
        if (!empty($user_id)) {
            $user_data = getUsersById($user_id);
            $balance = 0;
            $data = array();
            if (!empty($user_data)) {
                $balance = $user_data["balance"];
            }
            $resp_data["balance_info"] = $balance;
            $id_pending = array();
            $this->db->where("transaction_status", "queued");
            $this->db->where("payment_type", "payout");
            $this->db->where("user_id", $user_id);
            $gpending = $this->db->get("midtrans");
            if ($gpending && $gpending->num_rows() > 0) {
                foreach ($gpending->result() as $gpenval) {
                    $id_pending[] = array(
                        "reference_number" => $gpenval->transaction_id
                    );
                }
            }
            $resp_data["payout_pending"] = $id_pending;
            $this->db->order_by("transaction_time", "desc");
            $this->db->where("(user_id = " . $user_id . " OR book_id IN(SELECT book_id FROM book WHERE author_id = " . $user_id . "))");
            $this->db->where("(transaction_status = 'settlement' OR transaction_status = 'completed')");
            $result = $this->db->get("midtrans");
            if ($result && $result->num_rows()) {
                $jsresult = $result->result();
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "list history payouts shown";
                foreach ($jsresult as $jskey => $jsval) {
                    $book_id = $jsval->book_id;
                    $transaction_time = date("d M Y", strtotime($jsval->transaction_time));
                    $amount_trans = (double)$jsval->gross_amount;
                    $title = "";
                    $status_true = true;
                    $payment_type = ($jsval->payment_type == "payout") ? "penarikan" : "pembelian";
                    if ($user_id != $jsval->user_id) $payment_type = "penjualan";
                    // check if payout or transaction
                    if (!empty($book_id)) {
                        $books = $this->db->get_where("book", array("book_id" => $book_id));
                        if ($books && $books->num_rows() > 0) {
                            if($payment_type == "penjualan") $amount_trans = $books->row()->merchant_profit;
                            $title = $books->row()->title_book;
                            if($jsval->user_id == $user_id)  $status_true = false;
                        }
                    } else {
                        $banks = $this->db->get_where("banks", array("kode_bank" => $jsval->bank));
                        $bank_name = ($banks && $banks->num_rows() > 0) ? $banks->row()->bank_name : $jsval->bank;
                        $title = $bank_name . "-" . $jsval->va_numbers;
                    }

                    // check if penjualan
                    if($status_true == true){
                        $data[] = array(
                            "payment_type" => $payment_type,
                            "transaction_title" => $title,
                            "transaction_amount" => $amount_trans,
                            "transaction_time" => $transaction_time
                        );
                    }
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "list history empty";
            }
            $resp_data["transaction_history"] = $data;
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "user not authorized or empty";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    public function checkAccount_post()
    {
        $key = sethead();
        $user_id = getUserFromKey($key);
        $resp_data = array();
        $resp_code = REST_Controller::HTTP_OK;
        if (!empty($user_id)) {
            $user_data = getUsersById($user_id);
            $balance = 0;
            if (!empty($user_data)) {
                $balance = $user_data["balance"];
            }
            $resp_data["balance_info"] = $balance;
            $data = array();
            if (!empty($this->post("account_number"))) $this->db->where("account_no", $this->post("account_number"));
            if (!empty($this->post("id_acc"))) $this->db->where("id_acc", $this->post("id_acc"));
            $this->db->where("id_user", $user_id);
            $this->db->join("user_acc","user_acc.id_acc = user_acc_history.acc_id");
            $result = $this->db->get("user_acc_history");
            if ($result && $result->num_rows()) {
                $jsresult = $result->result();
                $resp_message = "account already exist";
                foreach ($jsresult as $jskey => $jsval) {
                    $banks = $this->db->get_where("banks", array("kode_bank" => $jsval->bank_code));
                    $bank_name = ($banks && $banks->num_rows() > 0) ? $banks->row()->bank_name : $jsval->bank_code;
                    $data = array(
                        "id_acc" => $jsval->id_acc,
                        "account_number" => $jsval->account_no,
                        "account_name" => $jsval->account_name,
                        "bank_name" => $bank_name,
                    );
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "account not exist, create one";
            }
            $resp_data["account"] = (object)$data;
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "user not authorized or empty";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    public function pendingPayout_get()
    {
        $key = sethead();
        $user_id = getUserFromKey($key);
        $resp_data = array();
        if (!empty($user_id)) {
            $this->db->where("transaction_status", "queued");
            $this->db->where("payment_type", "payout");
            $this->db->where("user_id", $user_id);
            $gpending = $this->db->get("midtrans");
            if ($gpending && $gpending->num_rows() > 0) {
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "show list pending transaction success";
                foreach ($gpending->result() as $gpenval) {
                    $transaction_status = $gpenval->transaction_status;
                    if ($transaction_status == "queued") $transaction_status = "Dalam proses...";
                    $fullname = "";
                    $this->db->join("user_acc","user_acc.id_acc = user_acc_history.acc_id");
                    $gacc = $this->db->get_where("user_acc_history", array("id_user" => $user_id, "account_no" => $gpenval->va_numbers, "bank_code" => $gpenval->bank));
                    if ($gacc && $gacc->num_rows() > 0) $fullname = $gacc->row()->account_name;
                    $transaction_time = date("d M Y", strtotime($gpenval->transaction_time));
                    $banks = $this->db->get_where("banks", array("kode_bank" => $gpenval->bank));
                    $bank_name = ($banks && $banks->num_rows() > 0) ? $banks->row()->bank_name : $gpenval->bank;
                    $resp_data[] = array(
                        "reference_number" => $gpenval->transaction_id,
                        "account_number" => $gpenval->va_numbers,
                        "account_name" => $fullname,
                        "bank_name" => $bank_name,
                        "amount" => $gpenval->gross_amount,
                        "payment_type" => $transaction_status,
                        "transaction_time" => $transaction_time
                    );
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "no pending transaction";
            }
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "user not authorized or empty";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    public function listAccount_get()
    {
        $key = sethead();
        $user_id = getUserFromKey($key);
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "";
        $resp_data = array();
        if (!empty($user_id)) {
            $this->db->join("user_acc","user_acc.id_acc = user_acc_history.acc_id");
            $cek_acc = $this->db->get_where("user_acc_history", array("id_user" => $user_id));
            if ($cek_acc && $cek_acc->num_rows() > 0) {
                foreach ($cek_acc->result() as $cekval) {
                    $banks = $this->db->get_where("banks", array("kode_bank" => $cekval->bank_code));
                    $bank_name = ($banks && $banks->num_rows() > 0) ? $banks->row()->bank_name : $cekval->bank_code;
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_message = "show history account success";
                    $resp_data[] = array(
                        "id_acc" => $cekval->id_acc,
                        "account_number" => $cekval->account_no,
                        "account_name" => $cekval->account_name,
                        "bank_name" => $bank_name,
                    );
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "no history account existed";
            }
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "user not authorized or empty";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    public function updateBeneficiaries_post()
    {
        $post = $this->post();
        $key = sethead();
        $user_id = getUserFromKey($key);
        if (!empty($user_id)) {
            $fullname = $post["account_name"];
            $email = "";
            $alias = "";
            $user_data = getUsersById($user_id);
            if (!empty($user_data)) {
                $fullname = (empty($fullname)) ? $user_data["fullname"] : "";
                $email = $user_data["email"];
                if (isset($post["bank_code"]) && !empty($post["bank_code"])) {
                    $alias = str_replace(' ', '', $fullname) . $post["bank_code"];
                    $alias = strtolower($alias);
                }
            }
            $request_body = array(
                "name" => $fullname,
                "account" => $post["account_number"],
                "bank" => $post["bank_code"],
                "alias_name" => $alias,
                "email" => $email
            );
            $api_url = $this->api_url . "/api/v1/beneficiaries/" . $alias;
            $result = $this->iris_api($api_url, "PATCH", $request_body);
            if ($result) {
                $jsresult = json_decode($result);
                if ($jsresult->status == "updated") {
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_message = "beneficiary updated";
                    $data_update = array(
                        "account_name" => $fullname,
                        "account_no" => $post["account_number"],
                        "bank_code" => $post["bank_code"]
                    );
                    $this->db->update("user_acc", $data_update, array("created_by" => $user_id));
                } else {
                    $resp_code = REST_Controller::HTTP_MULTIPLE_CHOICES;
                    $resp_message = $result;
                }
            } else {
                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $resp_message = $result;
            }
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "user not authorized or empty";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    public function listBeneficiaries_get()
    {
        $api_url = $this->api_url . "/api/v1/beneficiaries";
        $result = $this->iris_api($api_url, "GET");
        $data = array();
        if ($result) {
            $jsresult = json_decode($result);
            if ($jsresult->status == "created") {
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "beneficiary created";
                $data = json_decode($result);
            } else {
                $resp_code = REST_Controller::HTTP_MULTIPLE_CHOICES;
                $resp_message = $result;
            }
        } else {
            $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
            $resp_message = $result;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $data
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    private function iris_api($api_url, $request_type = "GET", $request_body = null, $server_key = '')
    {
        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode($this->server_key . ':')
            )
        );
        if ($request_type == "GET") {
            $curl_options[CURLOPT_CUSTOMREQUEST] = "GET";
            $curl_options[CURLOPT_POST] = false;
            $curl_options[CURLOPT_SSL_VERIFYPEER] = 0;
        } else {
            if ($request_type == "PATCH") $curl_options[CURLOPT_CUSTOMREQUEST] = "PATCH";
            $curl_options[CURLOPT_POST] = true;
            if (!empty($request_body)) $curl_options[CURLOPT_POSTFIELDS] = json_encode($request_body);
        }
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        if ($result) {
            return $result;
        } else {
            return curl_error($ch);
        }
    }
}