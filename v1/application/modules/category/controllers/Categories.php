<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
/**
* 
*/
class Categories extends REST_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->helper("baboo");
	}
	public function index()
	{
		echo "string";
	}
	function byCategory_post(){
		// echo "ass";
		$key = sethead();
		$category_post = str_replace('-', ' ', $this->post("category"));
		$user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_OK;
        $limit = REST_Controller::limit_timeline;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        if (!empty($this->post("fcm_id")) && !empty($this->post("device_id")) && !empty($user_id)) {
            $fcms = setDeviceFCM($user_id, $this->post("fcm_id"), $this->post("device_id"));
        }
        // $this->db->select("");
        // $this->db->join('book_category_master', 'book_category.cat_master_id = book_category_master.cat_master_id', 'inner');
        // $this->db->order_by("RAND()");
        // $this->db->where("cat_book LIKE '%".$category_post."%' OR book_category_master.cat_master_name LIKE '%".$category_post."%'");
        // $breadcrumbs = $this->db->get('book_category');

        $selysih = 0;
        $selisih = 0;
        $bukuteman = $this->db->query("SELECT COUNT(book_id) as totalbukuteman FROM book WHERE book.status_publish != 'draft' AND (book.author_id IN (SELECT is_follow FROM follows WHERE created_by = '" . $user_id . "') OR book.author_id = '" . $user_id . "')");
        $total_bukuteman = ($bukuteman && $bukuteman->num_rows() > 0) ? $bukuteman->row()->totalbukuteman : 0;
        $resp_data = array();
        $this->db->limit(REST_Controller::limit_timeline, $count);
        $this->db->order_by("RAND()");
        $this->db->where("book.status_publish != 'draft' AND (book.author_id IN (SELECT is_follow FROM follows WHERE created_by = '" . $user_id . "') OR book.author_id = '" . $user_id . "') AND cat_book LIKE '%".$category_post."%' OR book_category_master.cat_master_name LIKE '%".$category_post."%' ");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->join("book_category", "book_category.id_catbook = book.category_id");
        $this->db->join("book_category_master", "book_category.cat_master_id = book_category_master.cat_master_id");
        $this->db->select("book_id, title_book, author_id, fullname, prof_pict, category_id, book_type, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions");
        $timeline = $this->db->get("book");
        $setMax = REST_Controller::limit_timeline;
        $selasih = REST_Controller::limitNotFriend;
        $pageStop = round($selasih / $setMax);
        if ($selasih > $total_bukuteman) {
            $selysih = $selasih - $total_bukuteman;
            if ($setMax > $timeline->num_rows()) {
                $selisih = $setMax - $timeline->num_rows();
            }
        }
        $book_ids = array();
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($timeline && $timeline->num_rows() > 0) {
            // Set the response and exit
            foreach ($timeline->result() as $bokeys => $boval) {
                $is_bookmark = checkIsBookmark($boval->book_id, $user_id);
                $is_likes = checkIsLike($boval->book_id, $user_id);
                $getComment = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($boval->book_id));
                $res_par_text = getParagraphByBook($boval->book_id);
                $cat_id = 1;
                $publish_date = time_ago($boval->publish_at);
                $latest_update = $publish_date;
                if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                if (!empty($boval->category_id)) $cat_id = $boval->category_id;
                $category = getCategoryBook($cat_id);
                $book_ids[] = $boval->book_id;
                $dasray = array(
                    "book_id" => $boval->book_id,
                    "author_id" => $boval->author_id,
                    "author_name" => $boval->fullname,
                    "author_avatar" => (string)$boval->prof_pict,
                    "cover_url" => (string)$boval->file_cover,
                    "image_url" => (string)$boval->image_url,
                    "title_book" => (string)$boval->title_book,
                    "category" => $category,
                    "is_like" => $is_likes,
                    "is_bookmark" => $is_bookmark,
                    "book_type" => (int)$boval->book_type,
                    "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                    "latest_update" => $latest_update,
                    "publish_date" => time_ago($boval->publish_at),
                    "ptime" => strtotime($boval->publish_at),
                    "like_count" => (int)$boval->like_count,
                    "comment_count" => ($getComment && $getComment->num_rows() > 0) ? (int)$getComment->row()->comment_count : 0,
                    "view_count" => (int)$boval->view_count,
                    "share_count" => (int)$boval->share_count,
                    "desc" => $res_par_text
                );
                $resp_data[] = $dasray;
            }
        }
        //cek lanjutan
        if ($selisih > 0 && $this->post("count") <= $pageStop) {
            $not_in = "";
            $book_id = implode(",", $book_ids);
            if (!empty($book_id)) $not_in = " AND book_id NOT IN ($book_id)";
            $booksy = $this->db->query("SELECT book_id, title_book, author_id, fullname, prof_pict, category_id, book_type, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions FROM `book` INNER JOIN users ON book.author_id = users.user_id inner join book_category ON book.category_id = category.id_catbook INNER JOIN book_category_master ON book_category.cat_master_id = book_category_master.cat_master_id WHERE status_publish != 'draft' $not_in AND category.cat_book LIKE '%".$category_post."%' OR book_category_master.cat_master_name LIKE '%".$category_post."%' ORDER BY RAND() DESC LIMIT ?, ?", array($count, $selisih));
            if ($booksy && $booksy->num_rows() > 0) {
                foreach ($booksy->result() as $bokexs => $bovalx) {
                    $is_bookmarkx = checkIsBookmark($bovalx->book_id, $user_id);
                    $is_likesx = checkIsLike($bovalx->book_id, $user_id);
                    $getCommentx = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($bovalx->book_id));
                    $res_par_texty = getParagraphByBook($bovalx->book_id);
                    $cat_idx = 1;
                    $publish_datex = time_ago($bovalx->publish_at);
                    $latest_updatex = $publish_datex;
                    if (!empty($bovalx->latest_update) && $bovalx->latest_update != $bovalx->publish_at && $bovalx->status_publish == "publish") $latest_updatex = time_ago($bovalx->latest_update);
                    if (!empty($bovalx->category_id)) $cat_idx = $bovalx->category_id;
                    $categoryx = getCategoryBook($cat_idx);
                    $dasrayes = array(
                        "book_id" => $bovalx->book_id,
                        "author_id" => $bovalx->author_id,
                        "author_name" => $bovalx->fullname,
                        "author_avatar" => (string)$bovalx->prof_pict,
                        "cover_url" => (string)$bovalx->file_cover,
                        "image_url" => (string)$bovalx->image_url,
                        "title_book" => (string)$bovalx->title_book,
                        "category" => $categoryx,
                        "is_like" => $is_likesx,
                        "is_bookmark" => $is_bookmarkx,
                        "book_type" => (int)$bovalx->book_type,
                        "is_pdf" => (!empty($bovalx->is_pdf)) ? false : true,
                        "latest_update" => $latest_updatex,
                        "publish_date" => time_ago($bovalx->publish_at),
                        "ptime" => strtotime($bovalx->publish_at),
                        "like_count" => (int)$bovalx->like_count,
                        "comment_count" => ($getCommentx && $getCommentx->num_rows() > 0) ? (int)$getCommentx->row()->comment_count : 0,
                        "view_count" => (int)$bovalx->view_count,
                        "share_count" => (int)$bovalx->share_count,
                        "desc" => $res_par_texty
                    );
                    $resp_data[] = $dasrayes;
                }
            }
        }
        //check count
        if (count($resp_data) > 0) {
            usort($resp_data, function ($a, $b) {
                return (int)$b['ptime'] - (int)$a['ptime'];
            });
            foreach (array_keys($resp_data) as $mpakey) {
                unset($resp_data[$mpakey]["ptime"]);
                $populares = (object)array();
                $papakey = $mpakey + 1;
                if ($papakey == REST_Controller::popularBook_position) {
                    $desc = "Popular Books";
                    // $bookzy = getPopularBook();
                    $populares = (object)array("desc" => $desc, "data" => $bookzy);
                }
                $resp_data[$mpakey]["populars"] = $populares;
            }
            $resp_message = "show timeline success";
            $http_code_resp = REST_Controller::HTTP_OK;
        } else {
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "no timeline data";
        }

        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => array(
            	"breadcrumbs" => "ongoing",
                "timeline" => $resp_data
            )
        );
        // print_r($category_post);
        set_resp($responses, $http_response_header);
	}
}

/* End of file Categories.php */
/* Location: ./application/modules/category/controllers/Categories.php */