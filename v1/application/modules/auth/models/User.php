<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model
{
    function __construct()
    {
        $this->tableName = 'users';
        $this->primaryKey = 'user_id';
    }

    public function checkUserFB($data = array())
    {
        $this->db->select("user_id, role_id, email, fullname, prof_pict, email, oauth_uid, oauth_provider");
        $this->db->from($this->tableName);
        $this->db->where(array('email' => $data['email'], 'is_active' => 0));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        $users = array();
        if ($prevCheck > 0) {
            $dg = array();
            $ddg = array();
            $prevResult = $prevQuery->row();
            if(empty($prevResult->fullname)) $dg["fullname"] = $data["fullname"];
            if(empty($prevResult->prof_pict)) $dg["prof_pict"] = $data["prof_pict"];
            $dg["oauth_uid"] = $data["token"];
            $dg["oauth_provider"] = $data["oauth_provider"];
            if (isset($data["device_id"]) && !empty($data["device_id"])) $ddg["device_id"] = $data["device_id"];
            if (isset($data["fcm_id"]) && !empty($data["fcm_id"])) $ddg["fcm_id"] = $data["fcm_id"];
            $data['modified'] = date("Y-m-d H:i:s");
            $update = $this->db->update($this->tableName, $dg, array('user_id' => $prevResult->user_id));
            $this->db->select("user_id, role_id, email, fullname, IFNULL(date_of_birth, '') as date_of_birth, prof_pict, email, oauth_uid, oauth_provider, jk, IFNULL(address, '') as address,  IFNULL(phone, '') as phone, IFNULL(about_me, '') as about_me, balance, book_sold");
            $this->db->from("users");
            $this->db->where(array("user_id" => $prevResult->user_id));
            $getada = $this->db->get();
            $data_resp = array();
            if ($getada->num_rows() > 0) {
                $data_resp["user_id"] = $getada->row()->user_id;
                $data_resp["role_id"] = $getada->row()->role_id;
                $data_resp["email"] = $getada->row()->email;
                $data_resp["fullname"] = $getada->row()->fullname;
                $data_resp["date_of_birth"] = $getada->row()->date_of_birth;
                $data_resp["prof_pict"] = (string)$getada->row()->prof_pict;
                $data_resp["oauth_uid"] = (string)$getada->row()->oauth_uid;
                $data_resp["address"] = (string)$getada->row()->address;
                $data_resp["phone_number"] = (string)$getada->row()->phone;
                $data_resp["about_me"] = (string)$getada->row()->about_me;
                $data_resp["jk"] = (string)$getada->row()->jk;
                $data_resp["oauth_provider"] = (string)$getada->row()->oauth_provider;
                $data_resp["balance"] = (string)$getada->row()->balance;
                $data_resp["book_sold"] = (string)$getada->row()->book_sold;
                $data_resp["is_newuser"] = false;
                $data_resp["isFollow"] = false;

                //cek has pin
                $has_pin = false;
                $activated = false;
                $user_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND no_pin IS NOT NULL", array($getada->row()->user_id));
                if($user_pin && $user_pin->num_rows() > 0) $has_pin = true;
                $data_resp["has_pin"] = $has_pin;
                $uss_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND question1 IS NOT NULL AND question2 IS NOT NULL AND answer1 IS NOT NULL AND answer2 IS NOT NULL", array($getada->row()->user_id));
                if($uss_pin && $uss_pin->num_rows() > 0) $activated = true;
                $data_resp["is_activated"] = $activated;
                //count followers
                $followers = $this->db->get_where("follows", array("is_follow" => $getada->row()->user_id))->num_rows();
                $data_resp["followers"] = $followers;
                //count book made
                $book_made = $this->db->get_where("book", array("author_id" => $getada->row()->user_id, "status_publish" => "publish"))->num_rows();
                $data_resp["book_made"] = $book_made;
                //like book
                $this->db->where("author_id", $getada->row()->user_id);
                $this->db->join("likes", "likes.id_book = book.book_id");
                $ppl_like = $this->db->get("book")->num_rows();
                $data_resp["ppl_like"] = $ppl_like;
            }
            if (!empty($ddg)) {
                $this->db->select("device_id");
                $cekDuid = $this->db->get_where("device", array("uuid"=>$prevResult->user_id, "device_id"=>$data["device_id"]));
                if($cekDuid && $cekDuid->num_rows() > 0) $insd = $this->db->update("device", $ddg, array("uuid"=>$prevResult->user_id, "device_id"=>$data["device_id"]));
                else{
                    $ddg["uuid"] = $prevResult->user_id;
                    $insd = $this->db->insert("device", $ddg);
                }
                //end check
                if ($insd) {
                    $users["code"] = REST_Controller::HTTP_OK;
                    $users["message"] = "Login success";
                    $users["data"] = (object)$data_resp;
                } else {
                    $users["code"] = REST_Controller::HTTP_ACCEPTED;
                    $users["message"] = "Login failed";
                    $users["data"] = (Object)array();
                }
            } else {
                $userID = $prevResult->user_id;
                $users["code"] = REST_Controller::HTTP_OK;
                $users["message"] = "Login success";
                $users["data"] = (object)$data_resp;
            }
        } else {
            $ddg = array();
            $data['created'] = date("Y-m-d H:i:s");
            $data['modified'] = date("Y-m-d H:i:s");
            $insert = $this->db->insert($this->tableName, $data);
            $userID = $this->db->insert_id();
            $this->db->select("user_id, role_id, email, fullname, IFNULL(date_of_birth, '') as date_of_birth, prof_pict, email, oauth_uid, oauth_provider, jk, IFNULL(about_me, '') as about_me, IFNULL(address, '') as address, balance, book_sold");
            $this->db->from("users");
            $this->db->where(array("user_id" => $userID));
            $getada = $this->db->get();
            $data_resp = array();
            if ($getada->num_rows() > 0) {
                if ($userID > 1) {
                    $data_follow = array(
                        "is_follow" => 1,
                        "created_by" => $userID
                    );
                    $this->db->insert("follows", $data_follow);
                }
                $data_resp["user_id"] = $getada->row()->user_id;
                $data_resp["role_id"] = $getada->row()->role_id;
                $data_resp["email"] = $getada->row()->email;
                $data_resp["fullname"] = $getada->row()->fullname;
                $data_resp["date_of_birth"] = $getada->row()->date_of_birth;
                $data_resp["prof_pict"] = (string)$getada->row()->prof_pict;
                $data_resp["oauth_uid"] = (string)$getada->row()->oauth_uid;
                $data_resp["oauth_provider"] = (string)$getada->row()->oauth_provider;
                $data_resp["address"] = (string)$getada->row()->address;
                $data_resp["phone_number"] = (string)$getada->row()->phone;
                $data_resp["about_me"] = (string)$getada->row()->about_me;
                $data_resp["jk"] = (string)$getada->row()->jk;
                $data_resp["balance"] = (string)$getada->row()->balance;
                $data_resp["book_sold"] = (string)$getada->row()->book_sold;
                $data_resp["is_newuser"] = true;
                $data_resp["isFollow"] = false;
                //cek has pin
                $has_pin = false;
                $activated = false;
                $user_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND no_pin IS NOT NULL", array($getada->row()->user_id));
                if($user_pin && $user_pin->num_rows() > 0) $has_pin = true;
                $data_resp["has_pin"] = $has_pin;
                $uss_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND question1 IS NOT NULL AND question2 IS NOT NULL AND answer1 IS NOT NULL AND answer2 IS NOT NULL", array($getada->row()->user_id));
                if($uss_pin && $uss_pin->num_rows() > 0) $activated = true;
                $data_resp["is_activated"] = $activated;
                //count followers
                $followers = $this->db->get_where("follows", array("is_follow" => $getada->row()->user_id))->num_rows();
                $data_resp["followers"] = $followers;
                //count book made
                $book_made = $this->db->get_where("book", array("author_id" => $getada->row()->user_id, "status_publish" => "publish"))->num_rows();
                $data_resp["book_made"] = $book_made;
                //like book
                $this->db->where("author_id", $getada->row()->user_id);
                $this->db->join("likes", "likes.id_book = book.book_id");
                $ppl_like = $this->db->get("book")->num_rows();
                $data_resp["ppl_like"] = $ppl_like;
            }
            if (isset($data["device_id"]) && !empty($data["device_id"])) $ddg["device_id"] = $data["device_id"];
            if (isset($data["fcm_id"]) && !empty($data["fcm_id"])) $ddg["fcm_id"] = $data["fcm_id"];
            if ($userID) {
                $ddg["uuid"] = $userID;
                $insd = $this->db->insert("device", $ddg);
                $users["code"] = REST_Controller::HTTP_OK;
                $users["message"] = "Register success";
                $users["data"] = (Object)$data_resp;
            } else {
                $users["code"] = REST_Controller::HTTP_ACCEPTED;
                $users["message"] = "Register failed";
                $users["data"] = (Object)(array());
            }
        }
        return $users ? $users : FALSE;
    }

    public function checkUserGoogle($data = array())
    {
        $this->db->select("user_id, role_id, email, fullname, prof_pict, email, oauth_uid, oauth_provider");
        $this->db->from($this->tableName);
        $this->db->where(array('email' => $data['email'], 'is_active' => 0));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        $users = array();
        if ($prevCheck > 0) {
            $dg = array();
            $ddg = array();
            $prevResult = $prevQuery->row();
            if(empty($prevResult->fullname)) $dg["fullname"] = $data["fullname"];
            if(empty($prevResult->prof_pict)) $dg["prof_pict"] = $data["prof_pict"];
            $dg["oauth_uid"] = $data["token"];
            $dg["oauth_provider"] = $data["oauth_provider"];
            if (isset($data["device_id"]) && !empty($data["device_id"])) $ddg["device_id"] = $data["device_id"];
            if (isset($data["fcm_id"]) && !empty($data["fcm_id"])) $ddg["fcm_id"] = $data["fcm_id"];
            $data['modified'] = date("Y-m-d H:i:s");
            $update = $this->db->update($this->tableName, $dg, array('user_id' => $prevResult->user_id));
            $this->db->select("user_id, role_id, email, fullname, IFNULL(date_of_birth, '') as date_of_birth, prof_pict, email, oauth_uid, oauth_provider, jk, IFNULL(address, '') as address, IFNULL(about_me, '') as about_me, balance, book_sold");
            $this->db->from("users");
            $this->db->where(array("user_id" => $prevResult->user_id));
            $getada = $this->db->get();
            $data_resp = array();
            if ($getada->num_rows() > 0) {
                $data_resp["user_id"] = $getada->row()->user_id;
                $data_resp["role_id"] = $getada->row()->role_id;
                $data_resp["email"] = $getada->row()->email;
                $data_resp["fullname"] = $getada->row()->fullname;
                $data_resp["date_of_birth"] = $getada->row()->date_of_birth;
                $data_resp["prof_pict"] = (string)$getada->row()->prof_pict;
                $data_resp["oauth_uid"] = (string)$getada->row()->oauth_uid;
                $data_resp["oauth_provider"] = (string)$getada->row()->oauth_provider;
                $data_resp["address"] = (string)$getada->row()->address;
                $data_resp["phone_number"] = (string)$getada->row()->phone;
                $data_resp["about_me"] = (string)$getada->row()->about_me;
                $data_resp["jk"] = (string)$getada->row()->jk;

                $data_resp["balance"] = (string)$getada->row()->balance;
                $data_resp["book_sold"] = (string)$getada->row()->book_sold;
                $data_resp["is_newuser"] = false;
                $data_resp["isFollow"] = false;
                //cek has pin
                $has_pin = false;
                $activated = false;
                $user_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND no_pin IS NOT NULL", array($getada->row()->user_id));
                if($user_pin && $user_pin->num_rows() > 0) $has_pin = true;
                $data_resp["has_pin"] = $has_pin;
                $uss_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND question1 IS NOT NULL AND question2 IS NOT NULL AND answer1 IS NOT NULL AND answer2 IS NOT NULL", array($getada->row()->user_id));
                if($uss_pin && $uss_pin->num_rows() > 0) $activated = true;
                $data_resp["is_activated"] = $activated;
                //count followers
                $followers = $this->db->get_where("follows", array("is_follow" => $getada->row()->user_id))->num_rows();
                $data_resp["followers"] = $followers;
                //count book made
                $book_made = $this->db->get_where("book", array("author_id" => $getada->row()->user_id, "status_publish" => "publish"))->num_rows();
                $data_resp["book_made"] = $book_made;
                //like book
                $this->db->where("author_id", $getada->row()->user_id);
                $this->db->join("likes", "likes.id_book = book.book_id");
                $ppl_like = $this->db->get("book")->num_rows();
                $data_resp["ppl_like"] = $ppl_like;
            }

            if (!empty($ddg)) {
                $this->db->select("device_id");
                $cekDuid = $this->db->get_where("device", array("uuid"=>$prevResult->user_id, "device_id"=>$data["device_id"]));
                if($cekDuid && $cekDuid->num_rows() > 0) $insd = $this->db->update("device", $ddg, array("uuid"=>$prevResult->user_id, "device_id"=>$data["device_id"]));
                else{
                    $ddg["uuid"] = $prevResult->user_id;
                    $insd = $this->db->insert("device", $ddg);
                }
                // checking
                if ($insd) {
                    $users["code"] = REST_Controller::HTTP_OK;
                    $users["message"] = "Login success";
                    $users["data"] = (Object)$data_resp;
                } else {
                    $users["code"] = REST_Controller::HTTP_ACCEPTED;
                    $users["message"] = "Login failed";
                    $users["data"] = (Object)array();
                }
            } else {
                $userID = $prevResult->user_id;
                $users["code"] = REST_Controller::HTTP_OK;
                $users["message"] = "Login success";
                $users["data"] = (Object)$data_resp;
            }
        } else {
            $data['created'] = date("Y-m-d H:i:s");
            $data['modified'] = date("Y-m-d H:i:s");
            $insert = $this->db->insert($this->tableName, $data);
            $userID = $this->db->insert_id();
            $ddg = array();
            if (isset($data["device_id"]) && !empty($data["device_id"])) $ddg["device_id"] = $data["device_id"];
            if (isset($data["fcm_id"]) && !empty($data["fcm_id"])) $ddg["fcm_id"] = $data["fcm_id"];
            $this->db->select("user_id, role_id, email, fullname, IFNULL(date_of_birth, '') as date_of_birth, prof_pict, email, oauth_uid, oauth_provider, jk, IFNULL(address, '') as address, IFNULL(about_me, '') as about_me, balance, book_sold");
            $this->db->from("users");
            $this->db->where(array("user_id" => $userID));
            $getada = $this->db->get();
            $data_resp = array();
            if ($getada->num_rows() > 0) {
                if ($userID > 1) {
                    $data_follow = array(
                        "is_follow" => 1,
                        "created_by" => $userID
                    );
                    $this->db->insert("follows", $data_follow);
                }
                $data_resp["user_id"] = $getada->row()->user_id;
                $data_resp["role_id"] = $getada->row()->role_id;
                $data_resp["email"] = $getada->row()->email;
                $data_resp["fullname"] = $getada->row()->fullname;
                $data_resp["date_of_birth"] = $getada->row()->date_of_birth;
                $data_resp["prof_pict"] = (string)$getada->row()->prof_pict;
                $data_resp["oauth_uid"] = (string)$getada->row()->oauth_uid;
                $data_resp["oauth_provider"] = (string)$getada->row()->oauth_provider;
                $data_resp["address"] = (string)$getada->row()->address;
                $data_resp["phone_number"] = (string)$getada->row()->phone;
                $data_resp["about_me"] = (string)$getada->row()->about_me;
                $data_resp["jk"] = (string)$getada->row()->jk;
                $data_resp["balance"] = (string)$getada->row()->balance;
                $data_resp["book_sold"] = (string)$getada->row()->book_sold;
                $data_resp["is_newuser"] = true;
                $data_resp["isFollow"] = false;

                //cek has pin
                $has_pin = false;
                $activated = false;
                $user_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND no_pin IS NOT NULL", array($getada->row()->user_id));
                if($user_pin && $user_pin->num_rows() > 0) $has_pin = true;
                $data_resp["has_pin"] = $has_pin;
                $uss_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND question1 IS NOT NULL AND question2 IS NOT NULL AND answer1 IS NOT NULL AND answer2 IS NOT NULL", array($getada->row()->user_id));
                if($uss_pin && $uss_pin->num_rows() > 0) $activated = true;
                $data_resp["is_activated"] = $activated;
                //count followers
                $followers = $this->db->get_where("follows", array("is_follow" => $getada->row()->user_id))->num_rows();
                $data_resp["followers"] = $followers;
                //count book made
                $book_made = $this->db->get_where("book", array("author_id" => $getada->row()->user_id, "status_publish" => "publish"))->num_rows();
                $data_resp["book_made"] = $book_made;
                //like book
                $this->db->where("author_id", $getada->row()->user_id);
                $this->db->join("likes", "likes.id_book = book.book_id");
                $ppl_like = $this->db->get("book")->num_rows();
                $data_resp["ppl_like"] = $ppl_like;
            }
            if ($userID) {
                $ddg["uuid"] = $userID;
                $insd = $this->db->insert("device", $ddg);
                $users["code"] = REST_Controller::HTTP_OK;
                $users["message"] = "Register success";
                $users["data"] = (Object)$data_resp;
            } else {
                $users["code"] = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $users["message"] = "Register failed";
                $users["data"] = (Object)(array());
            }
        }
        return $users ? $users : FALSE;
    }

    public function delete_device($data_delete)
    {
        $sq = REST_Controller::HTTP_BAD_REQUEST;
        $message = "Logout failed";
        if (!empty($data_delete)) {
            $where = array("uuid" => $data_delete["user_id"], "device_id" => $data_delete["device_id"]);
            $gdq = $this->db->get_where("device", $where);
            if ($gdq->num_rows() > 0) {
                $dq = $this->db->delete("device", $where);
                if ($dq) $sq = REST_Controller::HTTP_OK;
                $message = "Logout success";
            } else {
                $message .= ", device not found";
            }
        }
        return array("code" => $sq, "message" => $message, "data" => (Object)array());
    }

    public function get_user()
    {
        $gq = $this->db->get($this->tableName);
        $sq = "users not exist";
        if ($gq->num_rows() > 0) {
            $sq = $gq->result();
        }
        return $sq;
    }

    public function get_userdata($userID)
    {
        $gq = $this->db->get_where($this->tableName, array("id" => $userID));
        $sq = "user not found";
        if ($gq->num_rows() > 0) {
            $sq = $gq->result();
        }
        return $sq;
    }

    public function insert_user($data_insert)
    {
        $stat_response = REST_Controller::HTTP_ACCEPTED;
        $user_id = "";
        $datas = array();
        $message = "";
        if (isset($data_insert) && !empty($data_insert)) {
            $cek_mail = $this->db->get_where("users", array("email" => $data_insert["email"]));
            if ($cek_mail->num_rows() == 0) {
                $data_insert["created"] = date("Y-m-d H:i:s");
                $data_insert["modified"] = date("Y-m-d H:i:s");
                $this->db->insert($this->tableName, $data_insert);
                $ins = $this->db->insert_id();
                if ($ins) {
                    if ($ins > 1) {
                        $data_follow = array(
                            "is_follow" => 1,
                            "created_by" => $ins
                        );
                        $this->db->insert("follows", $data_follow);
                    }
                    $stat_response = REST_Controller::HTTP_OK;
                    $user_id = $ins;
                    $this->db->select("user_id, role_id, email, fullname, IFNULL(date_of_birth, '') as date_of_birth, IFNULL(prof_pict, '') as prof_pict, email, IFNULL(oauth_uid, '') as oauth_uid, IFNULL(oauth_provider, '') as oauth_provider, IFNULL(address, '') as address, IFNULL(about_me, '') as about_me, IFNULL(phone, '') as phone_number, balance, book_sold, 0 as followers, 0 as book_made, 0 as ppl_like");
                    $this->db->from("users");
                    $this->db->where(array("user_id" => $user_id));
                    $getada = $this->db->get();
                    $data_resp = array();
                    if ($getada->num_rows() > 0) {
                        $data_resp = $getada->row();
                        $data_resp->is_newuser = true;
                        $data_resp->isFollow = false;
                        $data_resp->has_pin = false;
                        $data_resp->is_activated = false;
                        $data_resp->followers = 0;
                        $data_resp->book_made = 0;
                        $data_resp->ppl_like = 0;
                    }
                    $ddg = array();
                    if (isset($data_insert["device_id"]) && !empty($data_insert["device_id"])) {
                        $ddg["device_id"] = $data_insert["device_id"];
                    }
                    if (isset($data_insert["fcm_id"]) && !empty($data_insert["fcm_id"])) {
                        $ddg["fcm_id"] = $data_insert["fcm_id"];
                    }
                    $ddg["uuid"] = $user_id;
                    $insd = $this->db->insert("device", $ddg);
                    $datas = $data_resp;
                    $message = "register success";
                } else {
                    $stat_response = REST_Controller::HTTP_BAD_GATEWAY;
                    $message = "register failed";
                }
            } else {
                $stat_response = REST_Controller::HTTP_MULTIPLE_CHOICES;
                $message = "email already registered";
            }
        } else {
            $stat_response = REST_Controller::HTTP_BAD_REQUEST;
            $message = "register data cannot be empty";
        }
        //	$uuid = base64_encode($user_id);
        $response = array(
            "code" => $stat_response,
            "message" => $message,
            "data" => (Object)$datas
        );
        return $response;
    }

    public function update_user($data_update, $user_id, $data_tambahan = null)
    {
        $data_resp = array();
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        if (isset($user_id) && !empty($user_id)) {
            $data_update["modified"] = date("Y-m-d H:i:s");
            $upd = $this->db->update($this->tableName, $data_update, array($this->primaryKey => $user_id));
            if ($upd) {
                $stat_response = REST_Controller::HTTP_OK;
                $message = "update user success";
                $http_response_header = REST_Controller::HTTP_OK;
            } else {
                $stat_response = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $message = "user not updated";
            }
            if (!empty($data_tambahan)) {
                $sgtamb = $this->db->get("user_genre", array("id_users" => $user_id));
                if ($sgtamb->num_rows() > 0) {
                    $this->db->delete("user_genre", array("id_users" => $user_id));
                }
                $upd2 = 0;
                foreach ($data_tambahan as $usval) {
                    $this->db->insert("user_genre", $usval);
                    $insid2 = $this->db->insert_id();
                    $upd2 += $insid2;
                }
                if (!empty($upd2)) {
                    $stat_response = REST_Controller::HTTP_OK;
                    $message = "update user success";
                    $http_response_header = REST_Controller::HTTP_OK;
                } else {
                    $stat_response = REST_Controller::HTTP_BAD_REQUEST;
                    $message = "update success but update genre is failed";
                }
            }
        } else {
            $stat_response = REST_Controller::HTTP_NOT_FOUND;
            $message = "user not found";
        }
        $this->db->select("user_id, fullname, email, IFNULL(date_of_birth, '') as date_of_birth, IFNULL(jk, '') as jk, IFNULL(prof_pict, '') as prof_pict, IFNULL(address, '') as address, IFNULL(about_me, '') as about_me,  IFNULL(phone, '') as phone_number, balance, book_sold");
        $this->db->where("user_id", $user_id);
        $user = $this->db->get("users");
        if ($user->num_rows() > 0) {
            //count book made
            $data_resp = $user->row_array();
            //count followers
            $has_pin = false;
            $activated = false;
            $user_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND no_pin IS NOT NULL", array($user->row()->user_id));
            if($user_pin && $user_pin->num_rows() > 0) $has_pin = true;
            $data_resp["has_pin"] = $has_pin;
            $uss_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND question1 IS NOT NULL AND question2 IS NOT NULL AND answer1 IS NOT NULL AND answer2 IS NOT NULL", array($user->row()->user_id));
            if($uss_pin && $uss_pin->num_rows() > 0) $activated = true;
            $data_resp["is_activated"] = $activated;
            $data_resp["is_newuser"] = false;
            $data_resp["isFollow"] = false;
            $followers = $this->db->get_where("follows", array("is_follow" => $user->row()->user_id))->num_rows();
            $data_resp["followers"] = $followers;
            //count book made
            $book_made = $this->db->get_where("book", array("author_id" => $user->row()->user_id, "status_publish" => "publish"))->num_rows();
            $data_resp["book_made"] = $book_made;
            //like book
            $this->db->where("author_id", $user->row()->user_id);
            $this->db->join("likes", "likes.id_book = book.book_id");
            $ppl_like = $this->db->get("book")->num_rows();
            $data_resp["ppl_like"] = $ppl_like;

        }
        $response["responses"] = array(
            "code" => $stat_response,
            "message" => $message,
            "data" => (object)$data_resp
        );
        $response["http_response_code"] = $http_response_header;
        return $response;
    }

    public function delete_user($user_id)
    {
        $stat_response = REST_Controller::HTTP_ACCEPTED;;
        if (isset($user_id) && !empty($user_id)) {
            $del = $this->db->delete($this->tableName, array($this->primaryKey => $user_id));
            $stat_response = REST_Controller::HTTP_OK;
        }
        return $stat_response;
    }

    public function checkUser($data)
    {
        $this->db->select("id, fullname, email, role, jk, date_of_birth, role, is_active, oauth_provider, oauth_id, token, created, modified");
        $this->db->from($this->tableName);
        $this->db->where(array('username' => $data['username'], 'password' => md5($data['password'] . $this->config->item("encryption_key"))));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        $users = array();
        if ($prevCheck > 0) {
            $prevResult = $prevQuery->row();
            $data['modified'] = date("Y-m-d H:i:s");
            $update = $this->db->update($this->tableName, $data, array('id' => $prevResult['id']));
            $bbid = $prevResult->user_id;
            $users["status"] = REST_Controller::HTTP_OK;
            $users["userID"] = $bbid;
            $users["userdata"] = $prevResult;
            $users["response"] = "Login success";
        } else {
            $data['created'] = date("Y-m-d H:i:s");
            $data['modified'] = date("Y-m-d H:i:s");
            $insert = $this->db->insert($this->tableName, $data);
            $bbid = $this->db->insert_id();
            $users["status"] = REST_Controller::HTTP_OK;
            $users["userID"] = $bbid;
            $users["response"] = "Login success";
        }
        return $users ? $users : FALSE;
    }
}