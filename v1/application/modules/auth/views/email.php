<!DOCTYPE HTML>
<html>
<head>
    <meta charset='utf-8' />
    <meta name='viewport' content='width=device-width, initial-scale=1.0' />
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div class='col-md-12'>
        <div class='row'>
                	<div class='modal-dialog modal-sm'>
                    	<div class='modal-content'>
                    			    <div class='modal-header'>
                                    	   Hi <?php echo $name; ?>,
                            		</div>
                                    <br />
                            		<div class='modal-body'>
                                                Kami menerima permintaan anda untuk mereset password baboo.<br /> Silakan gunakan password <strong><?php echo $new_pass; ?></strong> untuk masuk ke baboo dan rubah password anda pada menu setting yang tersedia di halaman profile untuk keamanan akun anda.<br />Terimakasih<br /><br /><br />Cheers,<br /><strong>Baboo Digital Indonesia.</strong>
                            		</div><!--END of modalb-->
                        </div><!--END of mod-cont-->
                    </div><!--END of mod-Dia-->
        </div><!--END of row-->
    </div><!--END of row-->
</body>
</html>