<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 *  * User: Aditia@Baboo.id
 * Date: 16/08/2018
 * Time: 17.25
 */
class EpubCreator
{
    public function __construct()
    {

    }

    public function create_epub_free($bookId, $bookTitle, $bookInfo = array(), $bookCover, $imgLocation, $dataChapter = array())
    {
        $epub = new EpubPacker('../uploads/baboo-docs/epub-'.$bookId);
        $epub->init();

        $epub->setBookInfo($bookInfo);
        $epub->makeCover($bookCover);
        $imgLocation = $imgLocation;
        $i = 1;
        foreach( $dataChapter as $chapterTitle => $chapterBody )
        {
            $chapterBody = $epub->bodyFilter($chapterBody,$imgLocation);
            $epub->setData($chapterTitle,$chapterBody,$i);
            $chapterFileName = $epub->saveChapter();
            if( $chapterFileName )
            {
                $epub->addChapter();
                $i++;
            }
        }
        $packa = $epub->saveBook('../uploads/baboo-docs/',preg_replace("/[\s_]/", "-", $bookTitle).'-free.epub');
        echo $epub->showError();
        if( $packa )
        {
            $result = 200;
        }else{
            $result = 403;
        }
        return $result;
    }

    public function create_epub_full($bookId, $bookTitle, $bookInfo = array(), $bookCover, $imgLocation, $dataChapter = array())
    {
        $epub = new EpubPacker('../uploads/baboo-docs/epub-'.$bookId);
        $epub->init();

        $epub->setBookInfo($bookInfo);
        $epub->makeCover($bookCover);
        $imgLocation = $imgLocation;
        $i = 1;
        foreach( $dataChapter as $chapterTitle => $chapterBody )
        {
            $chapterBody = $epub->bodyFilter($chapterBody,$imgLocation);
            $epub->setData($chapterTitle,$chapterBody,$i);
            $chapterFileName = $epub->saveChapter();
            if( $chapterFileName )
            {
                $epub->addChapter();
                $i++;
            }
        }
        $packa = $epub->saveBook('../uploads/baboo-docs/',preg_replace("/[\s_]/", "-", $bookTitle).'.epub');
        echo $epub->showError();
        if( $packa )
        {
            $result = 200;
        }else{
            $result = 403;
        }
        return $result;
    }
}
